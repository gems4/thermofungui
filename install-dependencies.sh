#!/bin/bash
# Installing dependencies needed to build ThermoFunGUI 
#Needs gcc v.5 or higher and ArangoDB server locally installed

# installing dependencies of thermofun library
chmod u+x install-dependencies.sh
#./install-dependencies.sh

if [ "$(uname)" == "Darwin" ]; then
    # Do under Mac OS X platform
    #Needs Xcode and ArangoDB server locally installed
    #brew upgrade
    brew install lua
    EXTN=dylib

elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    #Needs gcc v.5 or higher and ArangoDB server locally installed
    #sudo apt-get update
    sudo apt-get install -y libcurl4-openssl-dev
    EXTN=so

    #sudo rm -f /usr/local/lib/liblua.a
    # Lua is a powerful, efficient, lightweight, embeddable scripting language
    # if no lua installed in /usr/local/lib/liblua.a (/usr/local/include)
    test -f /usr/local/lib/liblua.a || {

        # Building velocypack library
        mkdir -p ~/code && \
                cd ~/code && \
                curl -R -O http://www.lua.org/ftp/lua-5.4.2.tar.gz && \
                tar zxf lua-5.4.2.tar.gz && \
                cd lua-5.4.2 && \
                make "MYCFLAGS=-fPIC" linux && \
                sudo make install

        # Removing generated build files
        cd ~ && \
                 rm -rf ~/code
   }

fi


# Uncomment what is necessary to reinstall by force
#sudo rm -rf /usr/local/include/spdlog
#sudo rm -f /usr/local/lib/libvelocypack.a
#sudo rm -f /usr/local/lib/libjsonarango.$EXTN
#sudo rm -f /usr/local/lib/libjsonio17.$EXTN
#sudo rm -f /usr/local/lib/libpugixml.$EXTN
#sudo rm -f /usr/local/lib/libyaml-cpp.$EXTN
#sudo rm -f /usr/local/lib/libjsonimpex17.$EXTN
#sudo rm -f /usr/local/lib/libjsonui17.$EXTN
#sudo rm -rf /usr/local/include/nlohmann
#sudo rm -rf /usr/local/include/eigen3/Eigen/Eigen
#sudo rm -rf /usr/local/include/pybind11
#sudo rm -f /usr/local/lib/libChemicalFun.$EXTN
#sudo rm -f /usr/local/lib/libThermoFun.$EXTN
#sudo rm -f /usr/local/lib/libThermoHubClient.$EXTN

threads=3
QT_PATH=$1
echo $QT_PATH
BRANCH_JSON=master
BRANCH_TFUN=master
workfolder=${PWD}

USE_QT6=OFF
if [[ $QT_PATH == *"Qt/6"*  ]]; then
  USE_QT6=ON
fi
echo $USE_QT6

# spdlog
# if no spdlog installed in /usr/local/include/spdlog (copy only headers)
test -d /usr/local/include/spdlog || {

        # Building spdlog library
        mkdir -p ~/code && \
                cd ~/code && \
                git clone https://github.com/gabime/spdlog -b v1.11.0  && \
                cd spdlog/include && \
                sudo cp -r spdlog /usr/local/include

        # Removing generated build files
        cd ~ && \
                 rm -rf ~/code
}

# nlohmann/json library v. 3.6.1 or up
# if not installed in /usr/local/include/nlohmann)
test -f /usr/local/include/nlohmann/json.hpp || {

        # Getting nlohmann/json library
        mkdir -p ~/code && \
                cd ~/code && \
                git clone https://github.com/nlohmann/json.git && \
                cd json && \
                mkdir -p build && \
                cd build && \
                cmake .. -DCMAKE_BUILD_TYPE=Release -DJSON_BuildTests=OFF -DJSON_MultipleHeaders=ON && \
                sudo make install

        # Removing generated build files
        cd ~ && \
                 rm -rf ~/code
}

# Velocypack from ArangoDB (added for installing jsonArango database client)
# Installed as part jsonArango
# jsonArango database client
# if no jsonArango installed in /usr/local/lib/libjsonarango.a
test -f /usr/local/lib/libjsonarango.$EXTN || {

        # Building jsonio library
        mkdir -p ~/code && \
                cd ~/code && \
                git clone  --recurse-submodules https://bitbucket.org/gems4/jsonarango.git && \
                cd jsonarango && \
                mkdir -p build && \
                cd build && \
                cmake .. -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_BUILD_TYPE=Release && \
                make -j $threads && \
                sudo make install

        # Removing generated build files
        cd ~ && \
                 rm -rf ~/code
}

# JSONIO17 database client
# if no JSONIO installed in /usr/local/lib/libjsonio17.$EXTN (/usr/local/include/jsonio17)
test -f /usr/local/lib/libjsonio17.$EXTN || {

        # Building jsonio library
        mkdir -p ~/code && \
                cd ~/code && \
                git clone https://bitbucket.org/gems4/jsonio17.git -b $BRANCH_JSON && \
                cd jsonio17 && \
                mkdir -p build && \
                cd build && \
                cmake .. -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_BUILD_TYPE=$BuildType && \
                make && \
                sudo make install

        # Removing generated build files
        cd ~ && \
                 rm -rf ~/code
}

# pugixml
test -f /usr/local/lib/libpugixml.$EXTN || {

        # Building pugixml library
        mkdir -p ~/code && \
                cd ~/code && \
                git clone https://github.com/zeux/pugixml.git && \
                cd pugixml && \
                mkdir -p build && \
                cd build && \
                cmake .. -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=ON && \
                make -j $threads && \
                sudo make install

        # Removing generated build files
        cd ~ && \
                 rm -rf ~/code
}

# YAMLCPP
test -f /usr/local/lib/libyaml-cpp.$EXTN || {

        # Building yaml-cpp library
        mkdir -p ~/code && \
                cd ~/code && \
                git clone https://github.com/jbeder/yaml-cpp.git && \
                cd yaml-cpp && \
                mkdir -p build && \
                cd build && \
                cmake .. -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=ON -DYAML_CPP_BUILD_TOOLS=OFF -DYAML_CPP_BUILD_CONTRIB=OFF && \
                make -j $threads && \
                sudo make install

        # Removing generated build files
        cd ~ && \
                 rm -rf ~/code
}

## Thrift
## if no Thrift installed in /usr/local/lib/libthrift.a (/usr/local/include/thrift)
#test -f /usr/local/lib/libthrift.$EXTN || {
#
#        # Building thrift library
#        mkdir -p ~/code && \
#                cd ~/code && \
#                git clone http://github.com/apache/thrift && \
#                cd thrift && \
#                ./bootstrap.sh && \
#                ./configure  CXXFLAGS='-fPIC' --without-lua --without-qt5 && \
#                make -j $threads && \
#                sudo make install
#
#        # Removing generated build files
#        cd ~ && \
#                 rm -rf ~/code
#}

# JSONIMPEX (added for building ThermoMatch code)
# if no JSONIMPEX installed in /usr/local/lib/libjsonimpex17.$EXTN (/usr/local/include/jsonimpex17)
test -f /usr/local/lib/libjsonimpex17.$EXTN || {

	# Building jsonimpex library
	mkdir -p ~/code && \
		cd ~/code && \
		git clone https://bitbucket.org/gems4/jsonimpex17.git -b $BRANCH_JSON && \
		cd jsonimpex17 && \
		mkdir -p build && \
		cd build && \
		cmake .. -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_BUILD_TYPE=$BuildType && \
        	make && \
		sudo make install

	# Removing generated build files
	cd ~ && \
		 rm -rf ~/code
}

# JSONUI database client (added for building ThermoMatch code)
# if no JSONUI installed in /usr/local/lib/libjsonui17.$EXTN (/usr/local/include/jsonui17)
test -f /usr/local/lib/libjsonui17.$EXTN || {

        # Building jsonui library
        mkdir -p ~/code && \
                cd ~/code && \
                git clone https://bitbucket.org/gems4/jsonui17.git -b $BRANCH_JSON && \
                cd jsonui17 && \
                mkdir -p build && \
                cd build && \
                cmake .. -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_PREFIX_PATH=$QT_PATH -DJSONUI_USE_QT6=$USE_QT6 && \
                make -j $threads && \
                sudo make install

        # Removing generated build files
        cd ~ && \
                 rm -rf ~/code
}

# Eigen3 math library (added for building and installing xGEMS)
# if not installed in /usr/local/include/eigen3)
test -d /usr/local/include/eigen3/Eigen || {

	# Downloading and unpacking eigen3 source code into ~/code/eigen
    mkdir -p ~/code && \
	cd ~/code && \
	git clone git clone https://gitlab.com/libeigen/eigen.git -b 3.4.0 && \
	cd eigen && \
	mkdir -p build && \
	cd build && \
	cmake .. && \
	sudo make install

	# Removing generated build files
	cd ~ && \
	rm -rf ~/code
}

#Pybind11
test -d /usr/local/include/pybind11 || {

        # Building yaml-cpp library
        mkdir -p ~/code && \
                cd ~/code && \
                git clone https://github.com/pybind/pybind11.git && \
                cd pybind11 && \
                mkdir -p build && \
                cd build && \
                cmake .. -DPYBIND11_TEST=OFF && \
                make && \
                sudo make install

        # Removing generated build files
        cd ~ && \
                 rm -rf ~/code
}

# ChemicalFun library
# if no ChemicalFun installed in /usr/local/lib/ (/usr/local/include/ChemicalFun)
test -f /usr/local/lib/libChemicalFun.$EXTN || {

        # Building thermofun library
        mkdir -p ~/code && \
                cd ~/code && \
                git clone https://bitbucket.org/gems4/chemicalfun.git -b $BRANCH_TFUN  && \
                cd chemicalfun && \
                mkdir -p build && \
                cd build && \
                cmake .. -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DTFUN_BUILD_PYTHON=OFF && \
                make -j $threads && \
                sudo make install

        # Removing generated build files
        cd ~ && \
                 rm -rf ~/code
}

# ThermoFun library
# if no ThermoFun installed in /usr/local/lib/ (/usr/local/include/ThermoFun)
test -f /usr/local/lib/libThermoFun.$EXTN || {

        # Building thermofun library
        mkdir -p ~/code && \
                cd ~/code && \
                git clone https://bitbucket.org/gems4/thermofun.git -b $BRANCH_TFUN && \
                cd thermofun && \
                mkdir -p build && \
                cd build && \
                cmake .. -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DTFUN_BUILD_PYTHON=OFF && \
                make -j $threads && \
                sudo make install

        # Removing generated build files
        cd ~ && \
                 rm -rf ~/code
}

# ThermoHubClient library
# if no ThermoHubClient installed in /usr/local/lib/libThermoHubClient.a (/usr/local/include/ThermoHubClient)
test -f /usr/local/lib/libThermoHubClient.$EXTN || {

        # Building thermofun library
        mkdir -p ~/code && \
                cd ~/code && \
                git clone https://bitbucket.org/gems4/thermohubclient.git -b $BRANCH_TFUN && \
                cd thermohubclient && \
                mkdir -p build && \
                cd build && \
                cmake .. -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_BUILD_TYPE=$BUILD_TYPE && \
        make -j $threads && \
                sudo make install

        # Removing generated build files
        #cd ~ && \
        #	 rm -rf ~/code
}


if [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
   sudo ldconfig
fi
