#include <fstream>
#include <QFile>
#include <QFileInfo>
#include <QFileDialog>
#include <QMessageBox>

#include "jsonui17/VertexWidget.h"
#include "jsonui17/EdgesWidget.h"
#include "jsonui17/HelpMainWindow.h"
#include "jsonui17/preferences.h"
#include "jsonui17/models/schema_model.h"
#include "ThermoFunMainWindow.h"
#include "ui_ThermoFunMainWindow.h"
#include "ThermoFunWidgetNew.h"
#ifdef FROM_SRC
#include "../ThermoFun/Database.h"
#include "../ThermoFun/Element.h"
#include "../ThermoHubClient/ElementData.h"
#else
#include "ThermoFun/Database.h"
#include "ThermoFun/Element.h"
#include "ThermoHubClient/ElementData.h"
#endif

TThermoFunMainWin::TThermoFunMainWin(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TThermoFunMainWindow)
{
   ui->setupUi(this);
   //setWindowIcon(QIcon(":/main/GUI/Icons/thermomatch-logo-icon.png"));

   InitThermoFunLoggers();
   ui->centralWidget_->setStyleSheet(
    "border-image: url(:/main/GUI/Icons/BackgroundAqueosReactions900.png) stretch;");

   onCloseEvent = [&](QMainWindow* win)
   {
       auto it = jsonuiWindows.begin();
       while( it != jsonuiWindows.end() )
        if( *it == win ) // pointer
        {  jsonuiWindows.erase(it);
           break;
        } else
            it++;
       return true;
   };

   showWidget =  [=]( bool isVertex, const std::string& testschema,
           const std::string& recordkey, const jsonio17::DBQueryBase& query )
           {
               OpenNewWidget( isVertex, testschema, recordkey, query );
           };


   //set up main parameters
   setActions();

#ifndef WIN32
   new jsonui17::HelpMainWindow(nullptr);
#endif
}

TThermoFunMainWin::~TThermoFunMainWin()
{
    delete ui;
}

//  Connect all actions
void TThermoFunMainWin::setActions()
{
     // File
    connect( ui->actionNew_Element_Window , SIGNAL( triggered()), this, SLOT(CmNewElement()));
    connect( ui->actionNew_Substance_Window , SIGNAL( triggered()), this, SLOT(CmNewSubstance()));
    connect( ui->actionNew_Reaction_Window , SIGNAL( triggered()), this, SLOT(CmNewReaction()));
    connect( ui->actionNew_Reaction_Se_t_Window , SIGNAL( triggered()), this, SLOT(CmNewReactionSet()));
    connect( ui->actionNew_Data_Source_Window , SIGNAL( triggered()), this, SLOT(CmNewDataSource()));
    connect( ui->actionNew_ThermoDataSet_Window , SIGNAL( triggered()), this, SLOT(CmNewThermoDataSet()));
    connect( ui->actionNew_Edge_Editor_Window , SIGNAL( triggered()), this, SLOT(CmNewEdge()));
    connect( ui->actionE_xit, SIGNAL( triggered()), qApp, SLOT(quit()));

     // Help
    connect( ui->action_Help_About, SIGNAL( triggered()), this, SLOT(CmHelpAbout()));
    connect( ui->actionContents, SIGNAL( triggered()), this, SLOT(CmHelpContens()));
    connect( ui->actionAuthors, SIGNAL( triggered()), this, SLOT(CmHelpAuthors()));
    connect( ui->actionLicense, SIGNAL( triggered()), this, SLOT(CmHelpLicense()));
    connect( ui->actionPreferences, SIGNAL( triggered()), &jsonui17::uiSettings(), SLOT(CmSettingth()));

    // View
    connect( ui->action_Show_comments, SIGNAL( toggled(bool)), &jsonui17::uiSettings(), SLOT(CmShowComments(bool)));
    connect( ui->action_Display_enums, SIGNAL( toggled(bool)), &jsonui17::uiSettings(), SLOT(CmDisplayEnums(bool)));
    connect( ui->action_Edit_id, SIGNAL(toggled(bool)), &jsonui17::uiSettings(), SLOT(CmEditID(bool)));
    connect( ui->action_Keep_Data_Fields_Expanded, SIGNAL(toggled(bool)), &jsonui17::uiSettings(), SLOT(CmEditExpanded(bool)));

    //Data
//    connect( ui->actionGenerate_a_ThermoDataSet_record, SIGNAL( triggered()), this, SLOT(CmGenerateThermoDataSet()));
//    connect( ui->actionExport_ThermoDataSet, SIGNAL( triggered()), this, SLOT(CmExportThermoDataSet()));

    //Tools
    connect( ui->actionThermo_Fun_Properties_at_TP, SIGNAL( triggered()), this, SLOT(CmThermoFun()));
//    connect( ui->actionRecord_Calculator, SIGNAL( triggered()), this, SLOT(CmSelectElementsTest()));

//    connect( &jsonui17::uiSettings(), SIGNAL(dbChanged()), this, SLOT(setAllElements()));
//    connect( &jsonui17::uiSettings(), SIGNAL(schemaChanged()), this, SLOT(setAllElements()));

    connect( &jsonui17::uiSettings(), SIGNAL(dbChanged()), this, SLOT(updateDB()));
    connect( &jsonui17::uiSettings(), SIGNAL(schemaChanged()), this, SLOT(closeAll()));
    connect( &jsonui17::uiSettings(), SIGNAL(viewMenuChanged()), this, SLOT(updateViewMenu()));
    connect( &jsonui17::uiSettings(), SIGNAL(modelChanged()), this, SLOT(updateModel()));
    connect( &jsonui17::uiSettings(), SIGNAL(tableChanged()), this, SLOT(updateTable()));
    connect( &jsonui17::uiSettings(), SIGNAL(errorSettings(std::string)), this, SLOT(closeAll()));

}


/// Open new EdgesWidget  or JSONUIWidget  window
void TThermoFunMainWin::OpenNewWidget( bool isVertex, const std::string& testschema,
                        const std::string& recordkey, const jsonio17::DBQueryBase& query )
{
      jsonui17::BaseWidget* testWidget;
      if( isVertex )
      {
          testWidget = new jsonui17::VertexWidget( testschema/*, this*/ );
          connect( (jsonui17::VertexWidget*)testWidget, SIGNAL(vertexDeleted()), this, SLOT(onDeleteVertex()));
          connect( (jsonui17::VertexWidget*)testWidget, SIGNAL(graphLoaded()), this, SLOT(onLoadGraph()));
      }
      else
      {
          testWidget = new jsonui17::EdgesWidget( testschema, query/*, this*/ );
          connect( (jsonui17::EdgesWidget*)testWidget, SIGNAL(edgeDeleted()), this, SLOT(onDeleteEdge()));
          connect( (jsonui17::EdgesWidget*)testWidget, SIGNAL(graphLoaded()), this, SLOT(onLoadGraph()));
      }

      testWidget->setOnCloseEventFunction(onCloseEvent);
      testWidget->setShowWidgetFunction(showWidget);
      jsonuiWindows.push_back(testWidget);

      if( !recordkey.empty() )
      {    testWidget->openRecordKey(  recordkey  );
           //if( isVertex )
           //   testWidget->changeKeyList();
      }
      testWidget->show();
}


// Open new JSONUIWidget window
void TThermoFunMainWin::newSchemaWin(const char* testschema )
{
  try{
      OpenNewWidget( true, testschema, "" );
    }
   catch(jsonio17::jsonio_exception& e)
   {
       QMessageBox::critical( this, "jsonio_exception", e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void TThermoFunMainWin::closeEvent(QCloseEvent *e)
{
    if( jsonui17::HelpMainWindow::pDia )
       delete jsonui17::HelpMainWindow::pDia;
    closeAll();
    QWidget::closeEvent(e);
}

void TThermoFunMainWin::CmHelpContens()
{
    jsonui17::helpWin( "ThermoFun", "" );
}

void TThermoFunMainWin::CmHelpAbout()
{
    jsonui17::helpWin( "AboutThermoFun", "" );
}

void TThermoFunMainWin::CmHelpAuthors()
{
    jsonui17::helpWin( "AuthorsThermoFun", "" );
}

void TThermoFunMainWin::CmHelpLicense()
{
    jsonui17::helpWin( "LicenseThermoFun", "" );
}


/// Open new EdgesWidget window
void TThermoFunMainWin::CmNewEdge()
{
  try{
      OpenNewWidget( false, "", "" );
    }
   catch(jsonio17::jsonio_exception& e)
   {
       QMessageBox::critical( this, "jsonio_exception", e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Update menu for all windows
void TThermoFunMainWin::updateViewMenu()
{
    ui->action_Show_comments->setChecked( jsonui17::JsonSchemaModel::showComments );
    ui->action_Display_enums->setChecked(jsonui17::JsonSchemaModel::useEnumNames);
    ui->action_Edit_id->setChecked(jsonui17::JsonSchemaModel::editID );

    auto it = jsonuiWindows.begin();
    while( it != jsonuiWindows.end() )
    {  (*it)->updateViewMenu(); it++; }
}


/// Update model for all windows
void TThermoFunMainWin::updateModel()
{
    auto it = jsonuiWindows.begin();
    while( it != jsonuiWindows.end() )
    {  (*it)->updateModel(); it++; }
}


/// Update table for all windows
void TThermoFunMainWin::updateTable()
{
    auto it = jsonuiWindows.begin();
    while( it != jsonuiWindows.end() )
    {  (*it)->updateTable(); it++; }
}


/// Update DB for all windows
void TThermoFunMainWin::updateDB()
{
    if( jsonui17::HelpMainWindow::pDia )
        jsonui17::HelpMainWindow::pDia->resetDBClient();
    auto it = jsonuiWindows.begin();
    while( it != jsonuiWindows.end() )
    {  (*it)->updateDB(); it++; }
}

/// close all bson windows
void TThermoFunMainWin::closeAll()
{
    auto it = jsonuiWindows.begin();
    while( it != jsonuiWindows.end() )
    {  (*it)->close(); it = jsonuiWindows.begin(); }

    // jsonuiWindows.clear();
}

void TThermoFunMainWin::onLoadGraph()
{
    auto it = jsonuiWindows.begin();
    while( it != jsonuiWindows.end() )
    {
        jsonui17::EdgesWidget* edgWin = dynamic_cast<jsonui17::EdgesWidget*>(*it);
        if( edgWin )
          edgWin->updateQuery();
        else
          {
            jsonui17::VertexWidget* verWin = dynamic_cast<jsonui17::VertexWidget*>(*it);
            if( verWin )
              verWin->updateQuery();
          }
        it++;
    }
}


void TThermoFunMainWin::onDeleteEdge()
{
    auto it = jsonuiWindows.begin();
    while( it != jsonuiWindows.end() )
    {
       jsonui17::EdgesWidget* edgWin = dynamic_cast<jsonui17::EdgesWidget*>(*it);
       if( edgWin )
         edgWin->changeKeyList();
       it++;
    }
}

void TThermoFunMainWin::onDeleteVertex()
{
    auto it = jsonuiWindows.begin();
    while( it != jsonuiWindows.end() )
    {
        jsonui17::EdgesWidget* edgWin = dynamic_cast<jsonui17::EdgesWidget*>(*it);
        if( edgWin )
          edgWin->changeKeyList();
        else
          {
            jsonui17::VertexWidget* verWin = dynamic_cast<jsonui17::VertexWidget*>(*it);
            if( verWin )
              verWin->changeKeyList();
          }
        it++;
    }
}

// ----------------------------------------

void TThermoFunMainWin::CmThermoFun()
{
  try{
        ThermoFunWidgetNew* testWidget = new ThermoFunWidgetNew( this );

        testWidget->setOnCloseEventFunction(onCloseEvent);
        //testWidget->setShowWidgetFunction(showWidget);
        jsonuiWindows.push_back(testWidget);
        testWidget->show();
        testWidget->CmSelectThermoDataSet();

    }
   catch(jsonio17::jsonio_exception& e)
   {
       QMessageBox::critical( this, "jsonio_exception", e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


