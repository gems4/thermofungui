#ifndef TMAINWINDOW_H
#define TMAINWINDOW_H

#include <QMainWindow>
#include "jsonui17/BaseWidget.h"

namespace Ui {
class TThermoFunMainWindow;
}

class TThermoFunMainWin : public QMainWindow
{
    Q_OBJECT

    // Internal data
    std::list<jsonui17::BaseWidget*> jsonuiWindows;
    jsonui17::OnCloseEvent_f onCloseEvent;
    jsonui17::ShowVertexEdge_f showWidget;

    // Work functions
    void setActions();

    /// Open new JSONUIWidget window
    void newSchemaWin(const char* testschema );

    void closeEvent(QCloseEvent* e);


public slots:
    // update after change preferences
    void updateViewMenu();
    void updateModel();
    void updateTable();
    void updateDB();
    /// Close All JSONUIWidget windows
    void closeAll();

    // File
    void CmNewElement()
    { newSchemaWin( "VertexElement" ); }
    void CmNewSubstance()
    { newSchemaWin( "VertexSubstance" ); }
    void CmNewReaction()
    { newSchemaWin( "VertexReaction" ); }
    void CmNewReactionSet()
    { newSchemaWin( "VertexReactionSet" ); }
    void CmNewDataSource()
    { newSchemaWin( "VertexDataSource" ); }
    void CmNewThermoDataSet()
    { newSchemaWin( "VertexThermoDataSet" ); }
    void CmNewEdge();

    // Tools
    void CmThermoFun();

    // Help
    void CmHelpAbout();
    void CmHelpAuthors();
    void CmHelpLicense();
    void CmHelpContens();

    void onDeleteVertex();
    void onDeleteEdge();
    void onLoadGraph();

public:
    explicit TThermoFunMainWin(QWidget *parent = 0);
    ~TThermoFunMainWin();

    /// Open new EdgesWidget  or JSONUIWidget  window
    void OpenNewWidget( bool isVertex, const std::string& testschema,
                        const std::string& recordkey,
                        const jsonio17::DBQueryBase& query = jsonio17::DBQueryBase::emptyQuery() );

private:

    Ui::TThermoFunMainWindow *ui;
};

#endif // TMAINWINDOW_H
