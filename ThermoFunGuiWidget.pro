#-------------------------------------------------
#
# Project created by QtCreator 2016-02-08T16:01:53
#
#-------------------------------------------------

TARGET = ThermoFunDemoWidget
TEMPLATE = app

CONFIG += thread
CONFIG += c++2a
CONFIG += warn_on


!win32 {
    DEFINES += __unix
}
else
{
 #DEFINES  +=  IMPEX_OFF
}

QT   += core gui widgets
QT   += svg printsupport concurrent
QT   += charts
QT   += webenginewidgets

# Define the directory where source code is located
GUI_DIR       = ./ThermoFunGui
GUI_CPP       = $$GUI_DIR
GUI_H         = $$GUI_DIR

DEPENDPATH   += $$GUI_H
DEPENDPATH   += $$GUI_DIR

INCLUDEPATH   += $$GUI_H
INCLUDEPATH   += $$GUI_DIR

macx-clang {
  DEFINES += __APPLE__
  CONFIG -= warn_on
  CONFIG += warn_off
  INCLUDEPATH   += "/usr/local/include"
  DEPENDPATH   += "/usr/local/include"
  LIBPATH += "/usr/local/lib/"
}

win32 {
  INCLUDEPATH   += "C:\usr\local\include"
  DEPENDPATH   += "C:\usr\local\include"
  LIBPATH += "C:\usr\local\lib"
}

win32:LIBS +=  -ljsonio17-static -ljsonui17-static
!win32:LIBS += -ljsonio17 -ljsonui17
LIBS +=  -lChemicalFun -lThermoFun -lThermoHubClient

## end markdown

MOC_DIR = tmp
UI_DIR        = $$MOC_DIR
UI_SOURSEDIR  = $$MOC_DIR
UI_HEADERDIR  = $$MOC_DIR
OBJECTS_DIR   = obj

include($$GUI_DIR/ThermoFunGui.pri)

RESOURCES += \
    ThermoFunGuiWidget.qrc

#thrift -r -v --gen cpp

SOURCES += \
    Widget/main.cpp \
    Widget/ThermoFunMainWindow.cpp

HEADERS += \
    Widget/ThermoFunMainWindow.h

FORMS += \
    Widget/ThermoFunMainWindow.ui

#win32 {
#    QT   += webenginewidgets
#    QMAKE_CXXFLAGS_WARN_ON = -wd4068 -wd4138
#}

