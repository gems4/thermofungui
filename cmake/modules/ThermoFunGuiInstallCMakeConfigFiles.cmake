# The path where cmake config files are installed
set(THERMOFUNGUI_INSTALL_CONFIGDIR ${CMAKE_INSTALL_LIBDIR}/cmake/ThermoFunGui)

install(EXPORT ThermoFunGuiTargets
    FILE ThermoFunGuiTargets.cmake
    NAMESPACE ThermoFunGui::
    DESTINATION ${THERMOFUNGUI_INSTALL_CONFIGDIR}
    COMPONENT cmake)

include(CMakePackageConfigHelpers)

write_basic_package_version_file(
    ${CMAKE_CURRENT_BINARY_DIR}/ThermoFunGuiConfigVersion.cmake
    VERSION ${PROJECT_VERSION}
    COMPATIBILITY SameMajorVersion)

configure_package_config_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules/ThermoFunGuiConfig.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/ThermoFunGuiConfig.cmake
    INSTALL_DESTINATION ${THERMOFUNGUI_INSTALL_CONFIGDIR}
    PATH_VARS THERMOFUNGUI_INSTALL_CONFIGDIR)

install(FILES
    ${CMAKE_CURRENT_BINARY_DIR}/ThermoFunGuiConfig.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/ThermoFunGuiConfigVersion.cmake
    DESTINATION ${THERMOFUNGUI_INSTALL_CONFIGDIR})
