# ThermoFunGUI

### Install Dependencies

In the terminal ```~/thermofungui$```, execute the following commands:

```
#!bash
sudo ./install-dependencies.sh
```

### Compiling the C++ library

In the terminal ```~/thermofungui$```, execute the following commands (replace ```<path to Qt>``` e.g. ```/home/user/Qt/5.12.4/gcc_64/```):

```
#!bash
mkdir build && \
cd build && \
cmake .. -DCMAKE_PREFIX_PATH=<path to Qt> && \
make
``` 

This step will download, configure, build, and install all dependencies: `lua5.3-dev`, `libboost-all-dev`, `libcurl4-openssl-dev`, `libboost-test-dev`, `automake`, `flex`, `bison`, `libssl-dev`, `pugixml`, `yaml-cpp`,  `thrift`, `velocypack`, `jsonio`, `jsonimpex`, `jsonui`. The script will check if the dependencies are not already present at the default installation path ```/usr/local/``` and will only install them if not found. 
To reinstall dependencies open `install-dependencies.sh` files with a text editor. At the beginning of the script file commands for removing dependency library files are present but they are commented out with `#` symbol. Remove `#` for each dependency you wish to be reinstalled. 

For a global installation of the compiled libraries in your system, execute:

```
#!bash
sudo make install 
```

This will install thermoFunGui library and header files in the default installation directory of your system (e.g, ```/usr/local/``` ).

For a local installation, you can specify a directory path for the installed files as follows:

```
#!bash
cmake .. -DCMAKE_INSTALL_PREFIX=/home/username/local/
```
then execute:

```
sudo make install 
```

To compile thermoFunGui library in debug mode:

```
#!bash
cmake .. -DCMAKE_BUILD_TYPE=Debug
```
then execute:

```
sudo make install 
```

If compilation was successful the /Resources folder will be copied to the build folder. In this folder the necessary test files, connection to the arangodb database preferences, and data schemas are present. To change database connection without using the GUI, make changes in the ```Resources/fun-hubclient-config.json``` file.

## Buld and Run ThermoFunGui widget demo

To be able to build and run the ThemroFun GUI (graphical user batch) application demo, Qt needs to be installed.

* Download and install Qt 5.12.2 (https://www1.qt.io/download/) in your home directory ```~/Qt```. In the "Select components to install" menu select: Qt 5.12.2 with Desktop gcc 64-bit, Qt Charts, and Qt WebEngine

* For building using Qt Creator, use the ThermoFunDemoWidget.pro project file.

## Build and Run ThermoFun GUI on Windows 10

* Make sure you have git installed. If not, install it on Windows: https://git-scm.com/download/win.
* To download ThermoFun source code, using Windows Command Prompt go to C:/git/THERMOFUN and execute

```
git clone https://bitbucket.org/gems4/thermofun.git
```

## Prepare building tools

* ThermoFun GUI dependencies will be compiled using MSVC 2017 64 bit compiler. For this Visual Studio Community (2017) needs to be installed: 
https://docs.microsoft.com/en-us/visualstudio/install/install-visual-studio?view=vs-2017
At Step 4 - Select workloads, select Desktop development with C++ to be installed. On the individual components page check that also Windows 10 SDK is selected to be installed.
* In addition to MSVC 2017, Qt needs to be installed: https://www.qt.io/download in C:/Qt folder (Qt installation folder is used in further scripts, please use C:/Qt)!
Select with Qt 5.12.0 MSVC 2017 64-bit with Qt Charts, and Qt WebEngine.

### Install Dependencies

* For compiling the libraries that ThermoFun GUI is dependent on, three .bat scripts can be found in /thermofun. The process will several minutes. In a windows Command Prompt terminal go to C:/git/THERMOFUN/thermofun and run:

```
C:\git\THERMOFUN\thermofun>a-build-win-dependencies.bat
```

* This script builds curl and velocypack libraries, copies then in the C:\git\THERMOFUN\dependencies folder, creates buil-fun-gui folder and copies there the necessary resources files

```
C:\git\THERMOFUN\thermofun>b-build-win-boost.bat 
```

* This script builds the necessary boost libraries and copies then in the C:\git\THERMOFUN\dependencies folder

```
C:\git\THERMOFUN\thermofun>c-build-win-jsonio-jsonui.bat C:\Qt\5.12.3\msvc2017_64\bin
```

* This script builds jsonio and jsonui libraries, copies then in the C:\git\THERMOFUN\dependencies folder. Don't forget to use the corect Qt installation path.

### Compiling and the ThermoFun GUI demo in Qt Creator

* In Qt Creator open C:\git\THERMOFUN\thermofun\fungui\ThermoFunDemoGUI.pro
* Set the build folder to C:\git\THERMOFUN\build-fun-gui (release mode). NOT! C:\git\THERMOFUN\build-fun-gui\release 
* After the successful compilation try to run ThermoFun GUI from Qt Creator. All necessary dependencies and Resources should be already set in the right place. 

* Unsuccessful attempts could be due to unsuccessful compilation of dependencies, missing or not correctly copied lib or Resources files, etc. 
