# Require a certain version of cmake
cmake_minimum_required(VERSION 3.9)

# Set the name of the project
project(ThermoFunGui VERSION 0.2.0 LANGUAGES CXX)

# Set the cmake module path of the project
set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules")

# Use ccache to speed up repeated compilations
include(CCache)

# Define variables with the GNU standard installation directories
include(GNUInstallDirs)

# Ensure proper configuration if in a conda environment
include(CondaAware)

# Define which types of libraries to build
option(THERMOFUNGUI_BUILD_SHARED_LIBS "Build shared libraries." ON)
option(THERMOFUNGUI_BUILD_SHARED_LIBS "Build static libraries." ON)
option(JSONUI_USE_QT6 "Prefer Qt6 over Qt5 if available" ON)
#option(REFRESH_THIRDPARTY "Refresh thirdparty libraries." OFF)
option(THERMOFUNGUI_BUILD_WIDGET "Build example Qt application" ON)

# Modify the HUBCLIENT_BUILD_* variables accordingly to BUILD_ALL
if(THERMOFUNGUI_BUILD_ALL MATCHES ON)
    set(THERMOFUNGUI_BUILD_SHARED_LIBS          ON)
    set(THERMOFUNGUI_BUILD_STATIC_LIBS          ON)
    set(THERMOFUNGUI_BUILD_WIDGET               OFF)
endif()

# Used into conda only
if(DEFINED ENV{CONDA_PREFIX})
   option(USE_SPDLOG_PRECOMPILED "Use spdlog in compiled version" ON)
   set(JSONUI_USE_QT6 OFF)
else()
   option(USE_SPDLOG_PRECOMPILED "Use spdlog in compiled version" OFF)
endif()

# Set the default build type to Release
if(NOT CMAKE_BUILD_TYPE)
    # The build type selection for the project
    set(CMAKE_BUILD_TYPE Release CACHE STRING "Choose the build type for ${PROJECT_NAME}." FORCE)
    # The build type options for the project
    set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS Debug Release MinSizeRel RelWithDebInfo)
endif()

# Define if shared library should be build instead of static.
option(BUILD_SHARED_LIBS "Build shared libraries." ON)

# Set libraries to be compiled with position independent code mode (i.e., fPIC option in GNU compilers)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

# Set the C++ standard
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Set the list of compiler flags for MSVC compiler
if(${CMAKE_CXX_COMPILER_ID} STREQUAL MSVC)
    add_compile_options(
        /D_SCL_SECURE_NO_WARNINGS
        /D_CRT_SECURE_NO_WARNINGS=1
        /MP4
        /EHsc
        /Od
        /D_SILENCE_TR1_NAMESPACE_DEPRECATION_WARNING
        /DNOMINMAX
    )
endif()

# Find all ThermoFunGui dependencies
include(ThermoFunGuiFindDeps)


if(${CMAKE_CXX_COMPILER_ID} MATCHES "Clang")
   include_directories("/usr/local/include")
   link_directories("/usr/local/lib")
endif()
if(${CMAKE_CXX_COMPILER_ID} STREQUAL MSVC)
    include_directories("c:/usr/local/include")
    link_directories("c:/usr/local/lib")
endif()

# Set the ThermoFunGui source directory path
set(THERMOFUNGUI_SOURCE_DIR ${CMAKE_SOURCE_DIR}/ThermoFunGui)
# Set the include directories
include_directories(${THERMOFUNGUI_SOURCE_DIR})


# Build ThermoFunGui library
add_subdirectory(ThermoFunGui)

if(THERMOFUNGUI_BUILD_WIDGET MATCHES ON)
   add_subdirectory(Widget)
endif()


# Copy Resources folder
file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/Resources
        DESTINATION ${CMAKE_BINARY_DIR})


# Install the cmake config files that permit users to use find_package(ThermoFunGui)
include(ThermoFunGuiInstallCMakeConfigFiles)
