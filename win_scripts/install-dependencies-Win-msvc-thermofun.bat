@echo off
setlocal EnableDelayedExpansion 

set ROOT_DIR=%cd%
set LOCALINSTALL=C:/usr/local
set PROGFILES=%ProgramFiles%
if not "%ProgramFiles(x86)%" == "" set PROGFILES=%ProgramFiles(x86)%

REM Check if Visual Studio 2017 comunity is installed
set MSVCDIR="%PROGFILES%\Microsoft Visual Studio\2017\Community"
set VCVARSALLPATH="%PROGFILES%\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat"        
if exist %MSVCDIR% (
  if exist %VCVARSALLPATH% (
   	set COMPILER_VER="2017"
        set COMPILER_VER_NAME="Visual Studio 15 2017"
        echo Using Visual Studio 2017 Community
	goto setup_env
  )
)

REM Check if Visual Studio 2019 comunity is installed
set MSVCDIR="%PROGFILES%\Microsoft Visual Studio\2019\Community"
set VCVARSALLPATH="%PROGFILES%\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat"        
if exist %MSVCDIR% (
  if exist %VCVARSALLPATH% (
   	set COMPILER_VER="2019"
        set COMPILER_VER_NAME="Visual Studio 16 2019"
        echo Using Visual Studio 2019 Community
	goto setup_env
  )
)


echo No compiler : Microsoft Visual Studio 2017 or 2019 Community is not installed.
goto end

:setup_env


echo "%MSVCDIR%\VC\Auxiliary\Build\vcvarsall.bat"
call %MSVCDIR%\VC\Auxiliary\Build\vcvarsall.bat x64

rem Set the standard INCLUDE search path
rem
rem set INCLUDE=%LOCALINSTALL%\include;%INCLUDE%
rem set LIB=%LOCALINSTALL%\lib;%LIB%
rem set PATH=%LOCALINSTALL%\bin;%PATH%
echo %INCLUDE%


mkdir tmp_fun
cd tmp_fun

echo
echo ******                		 ******
echo ****** Compiling nlohmann/json         ******
echo ******                		 ******
echo


echo Get nlohmann/json from git...
git clone https://github.com/nlohmann/json.git
cd json

echo "Configuring..."
cmake -G%COMPILER_VER_NAME% -DCMAKE_BUILD_TYPE=Release -DJSON_BuildTests=OFF -DJSON_MultipleHeaders=ON -DCMAKE_INSTALL_PREFIX=%LOCALINSTALL% .. -S . -B build
echo "Building..."
cmake --build build  --target install
cd ..


echo
echo ******                		 ******
echo ****** Compiling thermofun         ******
echo ******                		 ******
echo


echo Get thermofun from git...
git clone https://bitbucket.org/gems4/thermofun.git
cd thermofun
git checkout origin/jsonio17-move

echo "Configuring..."
cmake -G%COMPILER_VER_NAME% -DCMAKE_BUILD_TYPE=Release -DTFUN_WASM=ON -DCMAKE_INSTALL_PREFIX=%LOCALINSTALL% .. -S . -B build
echo "Building..."
cmake --build build  --target install
cd ..


echo
echo ******                		 ******
echo ****** Compiling thermohubclient         ******
echo ******                		 ******
echo


echo Get thermohubclient from git...
git clone https://bitbucket.org/gems4/thermohubclient.git
cd thermohubclient
git checkout origin/jsonio17-move

echo "Configuring..."
cmake -G%COMPILER_VER_NAME% -DCMAKE_BUILD_TYPE=Release -DTHERMOHUBCLIENT_BUILD_PYTHON=OFF -DCMAKE_INSTALL_PREFIX=%LOCALINSTALL% .. -S . -B build
echo "Building..."
cmake --build build  --target install
cd ..\..

REM Housekeeping
rd /s /q tmp_fun



