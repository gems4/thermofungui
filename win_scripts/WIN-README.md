# ThermoFunGui


## Build and Run ThermoFunGui on Windows 10

* Make sure you have git installed. If not, install it on Windows: https://git-scm.com/download/win.
* To download ThermoFunGui source code, using Windows Command Prompt go to C:/gitThermoFun and execute

```sh
git clone https://bitbucket.org/gems4/thermofungui.git
```

## Prepare building tools mingw


## Prepare building tools MSVC

* ThermoFunGui dependencies will be compiled using MSVC 2017 64 bit compiler. For this Visual Studio Community (2017) needs to be installed: 
https://docs.microsoft.com/en-us/visualstudio/install/install-visual-studio?view=vs-2017
At Step 4 - Select workloads, select Desktop development with C++ to be installed. On the individual components page check that also Windows 10 SDK is selected to be installed.

* In addition to MSVC 2017, Qt needs to be installed: https://www.qt.io/download in C:/Qt folder (Qt installation folder is used in further scripts, please use C:/Qt)!
Select with Qt 5.15.2 MSVC 2019 64-bit with Qt Charts, and Qt WebEngine.

### Install Dependencies

For compiling the libraries that ThermoFun GUI is dependent on, two .bat scripts can be found in /win_script. The process will several minutes. In a windows Command Prompt terminal go to ~/thermofungui\win_script and run:

* This script builds jsonio17 and jsonui17 libraries, copies then in the C:\usr\local folder. Don't forget to use the corect Qt installation path.

```
~\thermofungui\win_script>install-dependencies-Win-msvc-jsonui17.bat C:\Qt\5.15.2\msvc2019_64
```

* This script builds the necessary thermofun libraries and copies then in the C:\usr\local folder

```
~\thermofungui\win_script>install-dependencies-Win-msvc-thermofun.bat
```





