#ifndef _model_reaction_h
#define _model_reaction_h

#include <memory>
#include <QAbstractItemModel>
#include <QTreeView>
#include <QItemDelegate>

// ThermoFun includes
#ifdef FROM_SRC
#include "../ThermoFun/ThermoProperties.h"
#endif
#ifndef FROM_SRC
#include "ThermoFun/ThermoProperties.h"
#endif

/// Definition of tree reaction line structure
class ReactionTree
{

public:

    ReactionTree( std::size_t andx, const  std::string& aname, std::vector<double>& avalues, std::vector<double>& aerrors, std::vector<Reaktoro_::Status>& astatus, ReactionTree* aparent ):
       ndx(andx), rsnames(aname), values(avalues), errors(aerrors), status(astatus), parent(aparent)
    {}

    ReactionTree( std::size_t andx, const  std::string& aname, ReactionTree* aparent = nullptr ):
        ndx(andx), rsnames(aname), parent(aparent)
    {}

    ~ReactionTree(){}

    std::size_t ndx;
    std::string rsnames;         // name of field
    std::vector<double> values;  // values of field
    std::vector<double> errors;  // errors of field
    std::vector<Reaktoro_::Status> status;     // status of value

    ReactionTree *parent;
    std::vector<std::unique_ptr<ReactionTree>> children;

};


//===========================================
// TReactionModel class
//===========================================


/*!
  \ class TReactionModel
  \ class for represents the data set and is responsible for fetching the data
  \ is needed for viewing and for writing back any changes.
  \ Reading/writing data from/to ReactionLine object
*/
class TReactionModel: public QAbstractItemModel
{
	Q_OBJECT

    QStringList hdData;
    ReactionTree* rootNode;

 public:

  TReactionModel(  ReactionTree* newRoot, QObject* parent = nullptr );
  ~TReactionModel(){}

  QModelIndex index(int row, int column, const QModelIndex& parent) const;
  QModelIndex parent(const QModelIndex& child) const;
  int rowCount ( const QModelIndex& parent ) const;     //ok
  int columnCount ( const QModelIndex& parent  ) const; // ok
  QVariant data ( const QModelIndex& index, int role ) const;
  //bool setData ( const QModelIndex& index, const QVariant& value, int role );
  QVariant headerData ( int section, Qt::Orientation orientation, int role ) const;
  Qt::ItemFlags flags ( const QModelIndex& index ) const;

  ReactionTree* lineFromIndex(const QModelIndex& index) const;

  void setModelData(ReactionTree* newRoot )
  {
      beginResetModel();
      rootNode = newRoot;
      endResetModel();
  }

};


/*!
  \ class TReactionView implements a tree view of reactions structure
  \ that displays items from a model. This class is used to
  \ provide standard tree views that were previously provided by
  \ the QTree class, but using the more flexible approach
  \ provided by Qt's model/view architecture.
*/
class TReactionView: public QTreeView
{
	Q_OBJECT

	void keyPressEvent(QKeyEvent* e);
    QMenu* contextMenu(const QModelIndex &index);

    QString createString();
    QString createHeader();
    void selectWithChildren(const QModelIndex& parIndex);
    void copyWithChildren( const QModelIndex& parIndex,
        const QModelIndexList& selIndexes, QString& clipText );

 protected slots:

    void slotPopupContextMenu(const QPoint& pos);
    void changeCurrent( int section );

 public slots:
    void CmHelp();
    void CmSelectRow();
    void CmSelectColumn();
    void CmSelectAll();
    void CmSelectGroup();
    void CmCopyData();
    void CmCopyDataHeader();

 public:

    TReactionView( QWidget * parent = nullptr );

};

#endif   // _model_reaction_h
