#ifndef SELECTELEMENTSDIALOG_H
#define SELECTELEMENTSDIALOG_H

#include <QDialog>
#include <QtWidgets>
#include "jsonui17/SelectDialog.h"

namespace Ui {
class SelectElements;
}

namespace ChemicalFun {
class ElementKey;
}

namespace ThermoFun {
class DatabaseClient;
}

struct SelectElementsDialogPrivate;
class ElementsWidget;

class SelectElementsDialog : public QDialog
{
    Q_OBJECT

    void  resetNextButton();
    void  resetBackButton();

    void  defineSource();
    void  defineElements( int sourcetdb );
    void  defineReactionSets();
    void  updateReactionSets();

protected slots:
    void objectChanged(){}

    void CmHelp();
    void CmOk();
    void CmNext();
    void CmBack();
    void CmReset();
    void CmSelectAll();

public:

    explicit SelectElementsDialog(const ThermoFun::DatabaseClient& hubClient, QWidget *parent = 0);
    ~SelectElementsDialog();

    void setData( int sourcetdb, const std::vector<ChemicalFun::ElementKey>& elementKeys,
                  const std::string& idRcSet );
    void allSelected( std::vector<ChemicalFun::ElementKey>& elementKeys ) const;
    int sourceTDB() const;
    std::string idReactionSet() const;

private:

    Ui::SelectElements *ui;

    // Step1
    std::shared_ptr<jsonui17::SelectTableContainer> cont_data;
    jsonui17::MatrixTable *pTable;
    // Step2
    /// Widget to selection of elements
    ElementsWidget* elmsWidget;
    // Step3
    /// ReactionSet selection table
    jsonui17::MatrixTable* rcsetTable = 0;

    /// Internal Data
    std::unique_ptr<SelectElementsDialogPrivate> pdata;

};

#endif // SELECTELEMENTSDIALOG_H
