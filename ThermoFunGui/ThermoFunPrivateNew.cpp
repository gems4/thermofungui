#ifdef _WIN32
#else
#include <sys/time.h>
#endif
#include <QScrollBar>
#include <QHeaderView>
#include "ThermoFunPrivateNew.h"
#include "ui_ThermoFunWidget.h"
#include "jsonio17/jsondump.h"
#include "jsonui17/preferences.h"

// ThermoFun includes
#include "ThermoFun/Database.h"
#include "ThermoFun/Batch/OutputBatch.h"
#include "ThermoFun/Common/ParseJsonToData.h"
#include "ThermoHubClient/TraversalData.h"
#include "ThermoHubClient/ReactionData.h"
#include "ThermoHubClient/ReactionSetData.h"


ThermoFunData::ThermoFunData()
{
  name = "ThermoFunTask1";
  comment = "write comment here...";
  calcStatus = "Select a thermodynamic dataset."; // status
  schemaName = "VertexSubstance";
  idThermoDataSet = "";
  sourceTDBs.clear();

  tPrecision = 0;
  unitsT = "C Celsius";
  pPrecision = 0;
  tppairs.push_back({25.,1.});
  unitsP = "b bar";
  properties.push_back("gibbs_energy");
  propertyUnits.push_back("J/mol");
  propertyPrecision.push_back(0);

  propertiesS.push_back("gibbs_energy");
  propertyUnitsS.push_back("J/mol");
  propertyPrecisionS.push_back(0);

  propertiesR.push_back("reaction_gibbs_energy");
  propertyUnitsR.push_back("J/mol");
  propertyPrecisionR.push_back(0);
}

void ThermoFunData::resetSchemaName( const std::string& newSchemaName )
{
    schemaName = newSchemaName;

    if (schemaName == "VertexSubstance")
    {
        properties = propertiesS;
        propertyUnits = propertyUnitsS;
        propertyPrecision = propertyPrecisionS;
    }
    if (schemaName == "VertexReaction")
    {
        properties = propertiesR;
        propertyUnits = propertyUnitsR;
        propertyPrecision = propertyPrecisionR;
    }
}


void ThermoFunData::toJsonNode( jsonio17::JsonFree& object ) const
{
    std::size_t ii;
    object[ "Name" ] = name;
    object[ "Description" ] = comment;
    object[ "SchemaName" ] = schemaName;
    object[ "ThermoDataSetId" ] = idThermoDataSet;
    object[ "SourceTDBs" ] = sourceTDBs;

    auto arr = jsonio17::JsonFree::array();
    for( ii=0; ii<elements.size(); ii++)
    {
        arr[ii]["0"] = elements[ii].Symbol();
        arr[ii]["1"] = elements[ii].Class();
        arr[ii]["2"] = elements[ii].Isotope();
    }
    object[ "ElementsList" ] = arr;

    object[ "TemperaturePrecision" ] = tPrecision;
    object[ "TemperatureUnits" ] = unitsT;
    object[ "PressurePrecision" ] = pPrecision;
    object[ "PressureUnits" ] = unitsP;

    auto arr2 = jsonio17::JsonFree::array();
    for( ii=0; ii<tppairs.size(); ii++)
    {
        arr2[ii]["T"] = tppairs[ii][0];
        arr2[ii]["P"] = tppairs[ii][1];
    }
    object[ "TemperaturePressurePoints" ] = arr2;

    object[ "PropertiesList" ] =  properties;
    object[ "PropertyUnits" ] =  propertyUnits[ii];
    object[ "PropertyPrecision" ] = propertyPrecision[ii];
}

void ThermoFunData::fromJsonNode( const jsonio17::JsonFree& object )
{
    ThermoFunData deflt;

    object.get_value_via_path( "Name", name, deflt.name );
    object.get_value_via_path( "Description", comment, deflt.comment );
    object.get_value_via_path( "SchemaName", schemaName, deflt.schemaName );
    object.get_value_via_path<std::string>( "ThermoDataSetId", idThermoDataSet, "" );
    object.get_value_via_path( "SourceTDBs", sourceTDBs, deflt.sourceTDBs );

    elements.clear();
    auto& arr  = object[ "ElementsList" ];
    if( arr.type() == jsonio17::JsonBase::Array )
    {
       std::string symbol;
       int class_, isotope;
       for( int ii=0; ii<arr.size(); ii++ )
       {
           auto& obj = arr[ii];
           if( !obj.get_value_via_path<std::string>( "0", symbol, "" ) )
             continue;
           if(!obj.get_value_via_path( "1", class_, 0 ) )
             continue;
           if(!obj.get_value_via_path( "2", isotope, 0 ) )
             continue;
           elements.push_back(ChemicalFun::ElementKey( symbol, class_, isotope ));
       }
    }

    object.get_value_via_path( "TemperaturePrecision", tPrecision, deflt.tPrecision );
    object.get_value_via_path( "TemperatureUnits", unitsT, deflt.unitsT );
    object.get_value_via_path( "PressurePrecision", pPrecision, deflt.pPrecision );
    object.get_value_via_path( "PressureUnits", unitsP, deflt.unitsP );

    // read tp pairs
    tppairs.clear();
    auto& arr2 = object[ "TemperaturePressurePoints" ];
    if( arr.type() != jsonio17::JsonBase::Array )
      tppairs =deflt.tppairs;
    else
    {
       double Ti, Pi;
       for( int ii=0; ii<arr2.size(); ii++ )
       {
           auto& obj = arr2[ii];
         obj.get_value_via_path( "T", Ti, 100. );
         obj.get_value_via_path( "P", Pi, 5. );
         tppairs.push_back({Ti, Pi});
       }
    }

    object.get_value_via_path( "PropertiesList", properties, deflt.properties );
    object.get_value_via_path( "PropertyUnits", propertyUnits, deflt.propertyUnits );
    object.get_value_via_path( "PropertyPrecision", propertyPrecision, deflt.propertyPrecision );
}

 std::string ThermoFunData::selection_to_string() const
 {
     std::string output;
     output +=  " idThermoDataSet: " + idThermoDataSet;
     output +=  "\n sourceTDBs: ";

     auto enumdef = jsonio17::ioSettings().Schema().getEnum( "SourceTDB" );
     if(enumdef != nullptr )
     {
      for( std::size_t ii=0; ii<sourceTDBs.size(); ii++ )
              output += enumdef->value2name(sourceTDBs[ii]) + "  ";
     }
     output +=  "\n";
     return output;
 }

// ThermoFunPrivateNew ------------------------------------------------------------------

ThermoFunPrivateNew::ThermoFunPrivateNew( ThermoFunWidgetNew* awindow ):
 QObject(awindow), window(awindow), ThermoHubClient( jsonui17::uiSettings().dbclient() )
{
    initWindow();
}

void ThermoFunPrivateNew::freeInternal()
{
    delete _TPlistTable;
    delete _PropertyTable;
    delete elementTable;
}

void ThermoFunPrivateNew::initWindow()
{
    // define all keys tables
    valuesTable = new jsonui17::MatrixTable( window );
    valuesTable->horizontalHeader()->setSectionResizeMode( QHeaderView::Interactive );
    valuesTable->setEditTriggers( QAbstractItemView::AnyKeyPressed );
    //valuesTable->setSortingEnabled(true);
    QObject::disconnect( valuesTable, SIGNAL(customContextMenuRequested(QPoint)),
           valuesTable, SLOT(slotPopupContextMenu(QPoint)));
    window->ui->keySplitter->insertWidget(0, valuesTable);
    window->ui->keySplitter->setStretchFactor(0, 4);
    substModel.reset(new ThermoViewModel( &ThermoHubClient.substData(), window ));
    reactModel.reset(new ThermoViewModel( &ThermoHubClient.reactData(), window ));
    if(valuesTable) {
       valuesTable->setModel(substModel->getModel());
    }

    // Elements list
    elementContainer = std::make_shared<ElementsDataContainer>( _data.elements );
    elementModel = new jsonui17::MatrixModel( elementContainer, window );
    elementTable = new jsonui17::MatrixTable( window, jsonui17::MatrixTable::tbShow );
    elementTable->setModel(elementModel);
    elementTable->horizontalHeader()->hide();
    elementTable->verticalHeader()->hide();
    elementTable->verticalHeader()->setStretchLastSection(true);
    //elementTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    int iHeight = 1.5*elementTable->verticalHeader()->sectionSize(0);
    //iHeight += elementTable->horizontalScrollBar()->height();
    elementTable->setMaximumHeight(iHeight);
    window->ui->verticalLayout_2->insertWidget(0, elementTable);

   // define ThermoFun data
   _TPContainer =  std::make_shared<TPContainer>( "T", std::vector<std::string>{ "T", "P" }, _data.tppairs );
   _TPlistTable  = new jsonui17::MatrixTable( window->ui->outWidget );
   jsonui17::MatrixDelegate* deleg = new jsonui17::MatrixDelegate();
   _TPlistTable->setItemDelegate(deleg);
   _TPlistModel = new jsonui17::MatrixModel( _TPContainer, window );
   _TPlistTable->setModel(_TPlistModel);
   _TPlistTable->horizontalHeader()->setSectionResizeMode( QHeaderView::Stretch/*Interactive*/ );
   window->ui->gridLayout_3->addWidget(_TPlistTable, 1, 0, 1, 1);

   _PropertyContainer = std::make_shared<TPropertyContainer>( "Property", _data.properties, _data.propertyUnits, _data.propertyPrecision );
   _PropertyTable  = new jsonui17::MatrixTable( window->ui->inWidget );
    deleg = new jsonui17::MatrixDelegate();
   _PropertyTable->setItemDelegate(deleg);
   _PropertyModel = new jsonui17::MatrixModel( _PropertyContainer, window );
   _PropertyTable->setModel(_PropertyModel);
   _PropertyTable->horizontalHeader()->setSectionResizeMode( QHeaderView::Stretch/*Interactive*/ );
    window->ui->gridLayout->addWidget(_PropertyTable, 7, 0, 1, 8);
}


// Edit part --------------------------------------------------------------------


// Reset internal data
void ThermoFunPrivateNew::updateData( const std::string& aThermoDataSet,
                                      const std::vector<int>& sourcetdbs,
                                      const std::vector<ChemicalFun::ElementKey>& elementKeys,
                                      const jsonio17::values_table_t&  substanceValues,
                                      const jsonio17::values_table_t&  reactionValues )
{
   _data.idThermoDataSet = aThermoDataSet;
   _data.sourceTDBs = move(sourcetdbs);
   _data.elements  = elementKeys;
   substValues = substanceValues;
   reactValues = reactionValues;
//    substModel->loadModeRecords( substanceValues );
//    reactModel->loadModeRecords( reactionValues );
    updateElementsModel();

}

void ThermoFunPrivateNew::updateElementsModel()
{
    if( elementModel)
    {
      elementModel->resetMatrixData();
      if( elementTable->horizontalScrollBar()->isVisible() )
      {
          int iHeight = elementTable->verticalHeader()->sectionSize(0);
          iHeight += elementTable->horizontalScrollBar()->height();
          elementTable->setMaximumHeight(iHeight);
      }
    }
    updateSelectMessage();
}

void ThermoFunPrivateNew::updateSelectMessage()
{
    window->ui->taskQuery->setText( _data.selection_to_string().c_str() );
}

void ThermoFunPrivateNew::reallocTP( int newsize )
{
  _data.tppairs.clear();
   for (unsigned int i = 0; i<newsize; i++ )
       _data.tppairs.push_back({0,0});
  _TPlistModel->resetMatrixData();
}

void ThermoFunPrivateNew::updateTP( const std::string& unitsT, const std::string& unitsP,
                   const std::vector<std::vector<double>>& tppairs)
{
    _data.unitsT = unitsT;
    _data.unitsP = unitsP;
    window->ui->pTunit->setCurrentText(unitsT.c_str());
    window->ui->pPunit->setCurrentText(unitsP.c_str());
    _data.tppairs = tppairs;
   _TPlistModel->resetMatrixData();
}


void ThermoFunPrivateNew::updateProperty( const std::vector<std::string>& properties )
{
    // store previous settings
    for (unsigned i = 0; i<_data.properties.size(); i++)
    {
        _data.mapUnits[_data.properties[i]] = _data.propertyUnits[i];
        _data.mapPrecision[_data.properties[i]] = _data.propertyPrecision[i];
    }
    _data.properties = properties;
    _data.propertyUnits.resize(_data.properties.size());
    _data.propertyPrecision.resize(_data.properties.size());
    for (unsigned i = 0; i<_data.properties.size(); i++)
    {
        _data.propertyUnits[i] = _data.mapUnits[_data.properties[i]];
        _data.propertyPrecision[i] = _data.mapPrecision[_data.properties[i]];
    }
   _PropertyModel->resetMatrixData();
}


void ThermoFunPrivateNew::linkChange()
{
  if( _data.schemaName == "VertexReaction") {
      if(valuesTable) {
         valuesTable->setModel(reactModel->getModel());
      }
  }
  else {
      if(valuesTable) {
         valuesTable->setModel(substModel->getModel());
      }
  }
}

void ThermoFunPrivateNew::typeChanged(const std::string& newSchemaName)
{
  if( newSchemaName != _curSchemaName )
  {
      if (_curSchemaName == "VertexSubstance")
      {
          _data.propertiesS = _data.properties;
          _data.propertyUnitsS = _data.propertyUnits;
          _data.propertyPrecisionS = _data.propertyPrecision;

          _data.properties = _data.propertiesR;
          _data.propertyUnits = _data.propertyUnitsR;
          _data.propertyPrecision = _data.propertyPrecisionR;
      }
      else
      {
          _data.propertiesR = _data.properties;
          _data.propertyUnitsR = _data.propertyUnits;
          _data.propertyPrecisionR = _data.propertyPrecision;

          _data.properties = _data.propertiesS;
          _data.propertyUnits = _data.propertyUnitsS;
          _data.propertyPrecision = _data.propertyPrecisionS;
      }
     _curSchemaName = newSchemaName;
     _data.resetSchemaName( _curSchemaName );
     _PropertyModel->resetMatrixData();
     //isDefaultQuery = !_data.query.empty();
     //window->ui->edgeQuery->setText(_data.query.getQueryString().c_str());
     linkChange();
  }
}

// Reset ThermoFun data
void ThermoFunPrivateNew::newThermoFunData( const ThermoFunData& newdata )
{
    _data = newdata;

   _curSchemaName = _data.schemaName;
    // update models
    updateElementsModel();
   _TPlistModel->resetMatrixData();
   _PropertyModel->resetMatrixData();
    linkChange();
    //isDefaultQuery = !_data.query.empty();
    // clear selection lists
    jsonio17::values_table_t emptytable;
    substModel->loadModeRecords( emptytable );
    reactModel->loadModeRecords( emptytable );
}


// Calc part ------------------------------

// extract init for calculation data
void ThermoFunPrivateNew::loadSubstData( const std::vector<size_t>& selNdx,
  std::vector<std::string>& aKeyList, std::vector<std::string>& substancesSymbols,
  std::vector<std::string>& substancesClass )
{
    //if(_data.schemaName != "VertexSubstance")
    //   return;

    const jsonio17::values_table_t& values= substModel->getValues();
    aKeyList.resize(selNdx.size());
    substancesSymbols.resize(selNdx.size());
    substancesClass.resize(selNdx.size());
    auto name_ndx = ThermoHubClient.substData().getDataName_DataIndex();

    for( std::size_t ii=0; ii<selNdx.size(); ii++ )
     {
        auto itValues = values[selNdx[ii]];
        substancesSymbols[ii] = itValues[name_ndx["symbol"]];
        substancesClass[ii] = itValues[name_ndx["class_"]];
        aKeyList[ii] = itValues[name_ndx["_id"]];
     }
}

void ThermoFunPrivateNew::loadReactData( const std::vector<size_t>& selNdx,
  std::vector<std::string>& aKeyList, std::vector<std::string>& reactionsSymbols )
{
    //if (_data.schemaName != "VertexReaction")
    //  return;
    const jsonio17::values_table_t& values= reactModel->getValues();
    auto name_ndx = ThermoHubClient.reactData().getDataName_DataIndex();
    aKeyList.resize(selNdx.size());
    reactionsSymbols.resize(selNdx.size());
    for( std::size_t ii=0; ii<selNdx.size(); ii++ )
    {
        auto itValues = values[selNdx[ii]];
        reactionsSymbols[ii] = itValues[name_ndx["symbol"]];
        aKeyList[ii] = itValues[name_ndx["_id"]];
    }
}

void ThermoFunPrivateNew::retrieveConnectedData( ThermoFun::VertexId_VertexType mapOfConnectedIds,
                                                 std::vector<std::string> &linkedSubstSymbols, std::vector<std::string> &linkedReactSymbols,
                                                 std::vector<std::string> &linkedSubstClasses, std::vector<std::string> &linkedSubstIds)
{
    std::string valDB;
    for (const auto& idType : mapOfConnectedIds)
    {
        if (idType.second == "reaction")
        {
            std::string symbol;
            std::string jsonrecord = ThermoHubClient.reactData().getJsonRecordVertex(idType.first);
            auto domdata = jsonio17::json::loads( jsonrecord );
            domdata.get_value_via_path<std::string>( "properties.symbol",  symbol, "" );
            // record = ThermoHubClient.reactData().getJsonRecordVertex(idType.first).second;
            // jsonio::bson_to_key( record.data, "properties.symbol",  symbol);
            linkedReactSymbols.push_back(symbol);
        }
        if (idType.second == "substance")
        {
            std::string symbol, class_;
            std::string jsonrecord = ThermoHubClient.substData().getJsonRecordVertex(idType.first);
            auto domdata = jsonio17::json::loads( jsonrecord );
            domdata.get_value_via_path<std::string>( "properties.symbol",  symbol, "" );
            domdata.get_value_via_path<std::string>( "properties.class_",  class_, "" );
            //  record = ThermoHubClient.substData().getJsonBsonRecordVertex(idType.first).second;
            //  jsonio::bson_to_key( record.data, "properties.symbol",  symbol);
            //  jsonio::bson_to_key( record.data, "properties.class_",  class_);
            /// !!!! class now struct
            linkedSubstClasses.push_back(class_);
            linkedSubstSymbols.push_back(symbol);
            linkedSubstIds.push_back(idType.first);
        }
    }
}

void ThermoFunPrivateNew::retrieveConnectedDataSymbols( const std::vector<std::string>& substKeys, const std::vector<std::string>& reactKeys, std::vector<std::string> &linkedSubstSymbols,
                                                           std::vector<std::string> &linkedReactSymbols, std::vector<std::string> &linkedSubstClasses, std::vector<std::string> &linkedSubstIds)
{
    linkedReactSymbols.clear(); linkedSubstSymbols.clear(); linkedSubstClasses.clear();

    retrieveConnectedData(ThermoHubClient.getTraversal().getMapOfConnectedIds(substKeys), linkedSubstSymbols, linkedReactSymbols, linkedSubstClasses, linkedSubstIds);
    retrieveConnectedData(ThermoHubClient.getTraversal().getMapOfConnectedIds(reactKeys), linkedSubstSymbols, linkedReactSymbols, linkedSubstClasses, linkedSubstIds);
//    react.getLinkedDataSymbols(reactKeys, linkedSubstSymbols, linkedReactSymbols, linkedSubstClasses);
//    subst.getLinkedDataSymbols(substKeys, linkedSubstSymbols, linkedReactSymbols, linkedSubstClasses);
}

void ThermoFunPrivateNew::setSubstanceLevel(std::string substSymbol, std::string level)
{
    ThermoHubClient.substData().setSubstanceLevel(substSymbol, level);
}

//void ThermoFunWidgetPrivate::setReactionLevel(string reactSymbol, string level)
//{
//    react.setReactionLevel(reactSymbol, level);
//}

MapSymbolMapLevelReaction ThermoFunPrivateNew::recordsMapLevelDefinesReaction_(ThermoFun::MapSubstSymbol_MapLevel_IdReaction mapSymbol_Level_idReact)
{
    MapSymbolMapLevelReaction recordsLevelReact;

    for (const auto& symbol_Level_idReact : mapSymbol_Level_idReact)
    {
        MapLevelReaction levelReact;
        for (const auto& level_idReact : symbol_Level_idReact.second)
        {
            levelReact[level_idReact.first] = ThermoFun::parseReaction(
                        ThermoHubClient.reactData().getJsonRecordVertex(level_idReact.second) );
        }
        recordsLevelReact[symbol_Level_idReact.first] = levelReact;
    }
    return recordsLevelReact;
}

MapSymbolMapLevelReaction ThermoFunPrivateNew::recordsMapLevelDefinesReaction()
{
    return recordsMapLevelDefinesReaction_(ThermoHubClient.substData().recordsMapLevelDefinesReaction());
}

MapSymbolMapLevelReaction ThermoFunPrivateNew::recordsMapLevelDefinesReaction(std::vector<std::string> connectedSubstIds, std::vector<std::string> connectedSubstSymbols)
{
    return recordsMapLevelDefinesReaction_(ThermoHubClient.substData().recordsMapLevelDefinesReaction(connectedSubstIds, connectedSubstSymbols));
}

ThermoFun::Database setSubstanceCalcType_(ThermoFun::Database tdb, ThermoFun::SubstanceThermoCalculationType::type type_)
{
    auto substances = tdb.getSubstances();
    for (auto substance : substances)
    {
        substance.setThermoCalculationType(type_);
        tdb.setSubstance(substance);
    }
    return tdb;
}

//MapSymbolMapLevelSubstances ThermoFunWidgetPrivate::recordsMapLevelTakesSubstances()
//{
//    return react.recordsMapLevelTakesSubstances();
//}

ThermoLoadData ThermoFunPrivateNew::loadData( std::vector<size_t> selNdx )
{
    ThermoLoadData data;

    try{

        if( isSubstances() )
            loadSubstData( selNdx, data.substKeys, data.substancesSymbols, data.substancesClass );
        else
            loadReactData( selNdx, data.reactKeys, data.reactionsSymbols );

//        retrieveConnectedDataSymbols( data.substKeys, data.reactKeys, data.linkedSubstSymbols,
//                                      data.linkedReactSymbols, data.linkedSubstClasses, data.linkedSubstIds);
    }
    catch(std::exception& e)
    {
        data.errorMessage = e.what();
    }
    return data;
}

std::string ThermoFunPrivateNew::calcData( ThermoLoadData loadedData, std::string solventSymbol,
                                      OutputOptions options )
{
    std::string returnMessage;
    QElapsedTimer time;
    time.start();

    try{

        std::vector<std::string> keys;

        keys.insert(keys.end(), loadedData.substKeys.begin(), loadedData.substKeys.end());
        keys.insert(keys.end(), loadedData.reactKeys.begin(), loadedData.reactKeys.end());

        auto tr = ThermoHubClient.getTraversal();

//        ThermoFun::VertexId_VertexType resultTraversal = tr.getMapOfConnectedIds(keys, ThermoHubClient.substData().getSubstSymbol_DefinesLevel());
        ThermoFun::Database tdb_ = tr.getDatabase(keys);// tr.getDatabaseFromMapOfIds(resultTraversal, ThermoHubClient.substData().getSubstSymbol_DefinesLevel());

        //    ThermoFun::Traversal tr(subst.getDB());
        //    ThermoFun::MapIdType resultTraversal = tr.getMapOfConnectedIds(keys, subst.mapSymbolLevel());
        //    ThermoFun::Database tdb_ = tr.getDatabaseFromMapOfIds(resultTraversal, subst.mapSymbolLevel());

        if (!options.calcSubstFromReact) // make all reactions to be calculated using the method in the record
            tdb_ = setSubstanceCalcType_(tdb_, ThermoFun::SubstanceThermoCalculationType::type::DCOMP);


        ThermoFun::ThermoBatch batchCalc (tdb_);
        batchCalc.setSolventSymbol(solventSymbol);

        ThermoFun::BatchPreferences op;
        if( options.output_number_format )
        {
            op.isFixed = true;
        } else
            op.isFixed = false;

        op.outputSolventProperties       = options.outSolventProp;
        op.reactionPropertiesFromReactants   = options.calcReactFromSubst;
        op.substancePropertiesFromReaction   = options.calcSubstFromReact;
        batchCalc.setBatchPreferences(op);



        if (_data.unitsP == "B kbar")
            batchCalc.setPropertyUnit("pressure","kbar");
        if (_data.unitsP == "p Pa")
            batchCalc.setPropertyUnit("pressure","Pa");
        if (_data.unitsP == "P MPa")
            batchCalc.setPropertyUnit("pressure","MPa");
        if (_data.unitsP == "A Atm")
            batchCalc.setPropertyUnit("pressure","atm");

        /// check !!!
        std::vector<std::string> solventPropNames = {"density", "alpha", "beta", "alphaT", "epsilon", "bornZ", "bornY", "bornQ", "bornX"};
        //    tpCalc.addSolventProperties(solventPropNames);

        std::map<std::string, int> precision = ThermoFun::defaultPropertyDigits;
        std::map<std::string, std::string> units = ThermoFun::defaultPropertyUnits;
        for (std::size_t jj = 0; jj <_data.properties.size(); jj++)
        {
            precision.at(_data.properties[jj]) = _data.propertyPrecision[jj];
            units.at(_data.properties[jj]) = _data.propertyUnits[jj];
        }

        precision.at("temperature") = _data.tPrecision;
        precision.at("pressure")    = _data.pPrecision;

        units.at("temperature") = "degC";
        units.at("pressure") = "bar";

        batchCalc.setDigits(precision);
        batchCalc.setUnits(units);
        std::vector<double> temperatures;
        std::vector<double> pressures;

        for (auto tppair : _data.tppairs )
        {
            temperatures.push_back(tppair[0]);
            pressures.push_back(tppair[1]);
        }

        if (_data.schemaName == "VertexReaction")
        {
            switch(options.out) {
            case outType::csv : batchCalc.thermoPropertiesReaction(_data.tppairs, loadedData.reactionsSymbols,
                                                                   _data.properties/*, calcReactFromSubst*/).toCSV(op.fileName);
                break;
            case outType::transposed : batchCalc.thermoPropertiesReaction(_data.tppairs, loadedData.reactionsSymbols,
                                                                          _data.properties/*, calcReactFromSubst*/).toCSVTransposed(op.fileName);
                break;
            case outType::propertygrid : batchCalc.thermoPropertiesReaction(temperatures, pressures, loadedData.reactionsSymbols,
                                                                            _data.properties/*, calcReactFromSubst*/).toCSVPropertyGrid(op.fileName);
                op.fileName = std::string(_data.properties[0]+"_"+op.fileName);
                break;
            }


            std::vector<std::string> reactionsEquations;
            for (const auto& symb : loadedData.reactionsSymbols)
            {
                reactionsEquations.push_back(tdb_.getReaction(symb).equation());
            }

            /// check !!!
            //        tpCalc.clearReactions();
            //        tpCalc.addReactions(reactionsEquations);

            //        ThermoFun::Output (tpCalc).toCSV("equations.csv");
        }

        if (_data.schemaName == "VertexSubstance")
            switch(options.out) {
            case outType::csv : batchCalc.thermoPropertiesSubstance( _data.tppairs, loadedData.substancesSymbols,
                                                                                 _data.properties/*, calcSubstFromReact*/).toCSV(op.fileName);
                break;
            case outType::transposed : batchCalc.thermoPropertiesSubstance( _data.tppairs, loadedData.substancesSymbols,
                                                                                        _data.properties/*, calcSubstFromReact*/).toCSVTransposed(op.fileName);
                break;
            case outType::propertygrid : batchCalc.thermoPropertiesSubstance(temperatures, pressures, loadedData.substancesSymbols,
                                                                                          _data.properties/*, calcSubstFromReact*/).toCSVPropertyGrid(op.fileName);
                op.fileName = std::string(_data.properties[0]+"_"+op.fileName);
                break;
            }

        _data.resultsFile = op.fileName;

        double delta_calc = time.elapsed()+ loadedData.time;
        returnMessage = "Calculation finished ("+ std::to_string(delta_calc/1000) + "s). Click view results.";
    }
    catch(std::exception& e)
    {
        returnMessage = std::string("Calculation finished with error: \n") + e.what();
    }
    return returnMessage;
}

//-----------------------------------------------------------------------

// temporaly functions
void ThermoFunPrivateNew::resetElementsintoRecord( bool isreact, const std::string& aKey )
{
  if(isreact)
   ThermoHubClient.reactData().resetRecordElements( aKey );
  else
   ThermoHubClient.reactSetData().resetRecordElements( aKey );
}

