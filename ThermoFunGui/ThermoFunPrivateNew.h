#ifndef THERMOFUNWIDGETPRIVATE_H
#define THERMOFUNWIDGETPRIVATE_H

#include <QObject>
#include <QtWidgets>
#include "ThermoFunWidgetNew.h"
#include "thermomodel.h"
#include "containers.h"
// ThermoFun includes
#ifdef FROM_SRC
#include "../ThermoFun/Batch/ThermoBatch.h"
#include "../ThermoFun/Reaction.h"
#include "../ThermoFun/Substance.h"
#include "../ThermoHubClient/DatabaseClient.h"
#include "../ThermoHubClient/SubstanceData.h"
#else
#include "ThermoFun/Batch/ThermoBatch.h"
#include "ThermoFun/Reaction.h"
#include "ThermoFun/Substance.h"
#include "ThermoHubClient/DatabaseClient.h"
#include "ThermoHubClient/SubstanceData.h"
#endif

using MapLevelReaction             = std::map< std::string, ThermoFun::Reaction>;
using MapLevelSubstances           = std::map< std::string, std::vector<ThermoFun::Substance>>;
using MapSymbolMapLevelReaction    = std::map< std::string, MapLevelReaction>;
using MapSymbolMapLevelSubstances  =std:: map< std::string, MapLevelSubstances>;

extern const int defSourcetdb;

/// ThermoFun data to calculation
struct ThermoFunData
{
     std::string name;    ///< Task (file) name
     std::string comment; ///< Task description
     std::string calcStatus; ///< ThermoFun colaculation status

     std::string schemaName;  ///< Selected schema ( "VertexSubstance" or "VertexReaction" )

    // selection data
    /// Selected ThermoDataSet
     std::string idThermoDataSet;
    /// Selected SourceTDBs
    std::vector<int> sourceTDBs;
    /// Define ELEMENTS table data
    std::vector<ChemicalFun::ElementKey> elements;

    // old selection data
    jsonio17::DBQueryBase _query;       ///< Current schema
     std::string _idReactionSet; ///< Id of reaction set,
                          ///< if not empty we load records only from ReactionSet
    int _sourcetdb;      ///< Current sourcetdb

    // TP data
     std::string unitsT;          ///< Current units of T
     std::string unitsP;          ///< Current units of P
    std::vector<std::vector<double>> tppairs;
    std::string output;
    std::string resultsFile;

    // Properties data
    std::vector< std::string> properties, propertiesS, propertiesR;    ///< Properties names list
    std::vector< std::string> propertyUnits, propertyUnitsS, propertyUnitsR; ///< Units of property
    std::vector<int>    propertyPrecision, propertyPrecisionS, propertyPrecisionR; ///< Units of property
    int pPrecision, tPrecision;

    // work data
    std::map< std::string, std::string> mapUnits = ThermoFun::defaultPropertyUnits;
    std::map< std::string, int>   mapPrecision = ThermoFun::defaultPropertyDigits;


    /// Default values to task
    ThermoFunData();

    /// Write current task to configuration file fileName
    void toJsonNode( jsonio17::JsonFree& object ) const;
    /// Read current task from configuration file fileName
    void fromJsonNode( const jsonio17::JsonFree& object );

    ///  Clear properies and query if schema changed
    void resetSchemaName( const  std::string& newSchemaName );

    /// Get  std::string definition of selection
    std::string selection_to_string() const;
};


class ThermoFunPrivateNew: public QObject
{

    Q_OBJECT

    friend class ThermoFunWidgetNew;

    ThermoFunWidgetNew* window;

   // Internal data
    std::string _curSchemaName = "";
   bool isDefaultQuery = false;

   /// Current project description
   ThermoFunData _data;
   /// Selected substances
   std::shared_ptr<ThermoViewModel>  substModel;
   /// Selected reactions
   std::shared_ptr<ThermoViewModel>  reactModel;

   /// Selected substances all
   jsonio17::values_table_t  substValues;
   /// Selected reactions all
   jsonio17::values_table_t  reactValues;

   // Window data  ------------------------------------

   // keys list data
   /// Selected data view
   jsonui17::MatrixTable* valuesTable = nullptr;

   // define  ELEMENTS table model
   std::shared_ptr<ElementsDataContainer> elementContainer;
   jsonui17::MatrixTable *elementTable = nullptr;
   jsonui17::MatrixModel* elementModel = nullptr;

   //ThermoFun data to edit;
   std::shared_ptr<TPContainer> _TPContainer;
   jsonui17::MatrixTable*  _TPlistTable = nullptr;
   jsonui17::MatrixModel*  _TPlistModel = nullptr;

   std::shared_ptr<TPropertyContainer> _PropertyContainer;
   jsonui17::MatrixTable*  _PropertyTable = nullptr;
   jsonui17::MatrixModel*  _PropertyModel = nullptr;

   // internal functions
   void initWindow();
   void freeInternal();
   void updateElementsModel();
   void updateSelectMessage();
   void linkChange();

   MapSymbolMapLevelReaction recordsMapLevelDefinesReaction_(ThermoFun::MapSubstSymbol_MapLevel_IdReaction mapSymbol_Level_idReact);

public slots:

    void pPChanged(int val)
    {
      _data.pPrecision = val;
    }
    void tPChanged(int val)
    {
      _data.tPrecision = val;
    }
    void TUnitsChanged(const QString& text)
    {
      _data.unitsT = text.toStdString();
    }
    void PUnitsChanged(const QString& text)
    {
      _data.unitsP = text.toStdString();
    }
    void POutputChanged(const QString& text)
    {
      _data.output = text.toStdString();
    }
    void nameChanged(const QString& text)
    {
      _data.name = text.toStdString();
    }
    void commentChanged(const QString& text)
    {
      _data.comment = text.toStdString();
    }

    void retrieveConnectedData(std::map<std::string, std::string> mapOfConnectedIds, std::vector<std::string> &linkedSubstSymbols,
     std::vector<std::string> &linkedReactSymbols, std::vector<std::string> &linkedSubstClasses, std::vector<std::string> &linkedSubstIds);


public:

   ThermoFun::DatabaseClient ThermoHubClient;

   ThermoFunPrivateNew( ThermoFunWidgetNew* awindow );

   ~ThermoFunPrivateNew()
   {
     freeInternal();
   }

   const ThermoFunData& data() const
   {
     return _data;
   }

   bool isSubstances() const
   {
     return _data.schemaName != "VertexReaction";
   }

   const jsonio17::values_table_t&  getValues( bool isSubst ) const
   {
     if( isSubst )
       return substModel->getValues();
     else
       return reactModel->getValues();
   }

    std::string getThermoPropertiesName() const
   {
       std::size_t pos = std::string("Vertex").length();
       std:: string schemaName = "ThermoProperties" + _data.schemaName.substr (pos);
       return schemaName;
   }

   void newThermoFunData( const ThermoFunData& newdata );

   /// Reset internal data (after select dialog )
   void updateData( const std::string& aThermoDataSet,
                    const std::vector<int>& sourcetdbs,
                    const std::vector<ChemicalFun::ElementKey>& elementKeys,
                    const jsonio17::values_table_t&  substanceValues,
                    const jsonio17::values_table_t&  reactionValues );

   void reallocTP( int newsize );
   void updateTP( const std::string& unitsT, const std::string& unitsP,
                      const std::vector<std::vector<double>>& tppairs);
   void updateProperty( const std::vector<std::string>& properties );

   void typeChanged(const std::string& newSchemaName);

   // calc functions
   void loadSubstData( const std::vector<size_t>& selNdx,
     std::vector<std::string>& aKeyList, std::vector<std::string>& substancesSymbols,
     std::vector<std::string>& substancesClass );
   void loadReactData( const std::vector<size_t>& selNdx,
     std::vector<std::string>& aKeyList, std::vector<std::string>& reactionsSymbols );

   std::map<std::string, std::size_t>  getSubstDataIndex()
   {
       return ThermoHubClient.substData().getDataName_DataIndex();
   }

   void retrieveConnectedDataSymbols(const std::vector<std::string>& substKeys, const std::vector<std::string>& reactKeys,
                                     std::vector<std::string> &linkedSubstSymbols, std::vector<std::string> &linkedReactSymbols,
                                     std::vector<std::string> &linkedSubstClasses, std::vector<std::string> &linkedSubstIds);

   MapSymbolMapLevelReaction   recordsMapLevelDefinesReaction();
   MapSymbolMapLevelReaction   recordsMapLevelDefinesReaction(std::vector<std::string> connectedSubstIds , std::vector<std::string> connectedSubstSymbols);

   void setSubstanceLevel( std::string substSymbol, std::string level);

   ThermoLoadData loadData( std::vector<size_t> selNdx );

   std::string calcData(ThermoLoadData loadedData, std::string solventSymbol,
                    OutputOptions options);

   // temporaly functions
   void resetElementsintoRecord( bool isreact, const std::string& aKey );

};



#endif // THERMOFUNWIDGETPRIVATE_H
