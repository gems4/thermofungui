#ifndef ELEMENTSWIDGET_H
#define ELEMENTSWIDGET_H

#include <set>
#include <QWidget>
#include <QButtonGroup>
#include <spdlog/spdlog.h>

namespace ChemicalFun
{
class ElementKey;
class ElementValues;
}

namespace Ui {
class ElementsWidget;
}

/// Default logger for ThermoFunGui library
extern std::shared_ptr<spdlog::logger> fungui_logger;

class ElementsWidget : public QWidget
{
    Q_OBJECT

    void  setUpButtonGroups();
    void  EmptyData();
    void  ResetData();

public slots:
    void CmSelectAll();
    void CmReset()
    {
      ResetData();
    }
    void CmResetREE();
    void CmResetACT();
    void SetIsotopes();

public:
    explicit ElementsWidget(QWidget *parent = nullptr);
    ~ElementsWidget();

    void  setElementList(const std::vector<ChemicalFun::ElementKey>& elements,
                         const std::map<ChemicalFun::ElementKey, ChemicalFun::ElementValues>& all_elements);
    void  selectElementList(const std::vector<ChemicalFun::ElementKey>& elements,
                            const std::map<ChemicalFun::ElementKey, ChemicalFun::ElementValues>& all_elements);
    void allSelected( std::vector<ChemicalFun::ElementKey>& elementKeys ) const;

private:
    Ui::ElementsWidget *ui;

    std::vector<int> aBtmId1;         // lists correspondanse betvin bgElem
    std::vector<ChemicalFun::ElementKey> aICkey1;  // buttons and IComp records
    std::vector<int> aBtmId2;         // lists correspondanse betvin bgOther
    std::vector<ChemicalFun::ElementKey> aICkey2;  // buttons and IComp records
    QButtonGroup* bgElem;
    QButtonGroup* bgOther;

};

#endif // ELEMENTSWIDGET_H
