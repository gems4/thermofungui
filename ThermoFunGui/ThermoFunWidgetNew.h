//  This is ThermoFun library+API (https://bitbucket.org/gems4/ThermoFun)
//
/// \file ThermoFunWidget.h
/// ThermoFunWidget - Widget to work with ThermoFun data
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// jsonio (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io);
// Qwtplot (http://qwt.sourceforge.net).
//

#ifndef ThermoFunWIDGETNEW_H
#define ThermoFunWIDGETNEW_H

#include <QtCore/QFutureWatcher>
#include "jsonui17/BaseWidget.h"

namespace jsonui17 {
class TableEditWidget;
}


namespace Ui {
class ThermoFunWidget;
}

struct ThermoFunData;
class ThermoFunPrivateNew;
class WaitingSpinnerWidget;
void InitThermoFunLoggers();

struct ThermoLoadData
{
    std::vector<std::string> substKeys;
    std::vector<std::string> reactKeys;
    std::vector<std::string> substancesSymbols;
    std::vector<std::string> substancesClass;
    std::vector<std::string> reactionsSymbols;
    std::vector<std::string> linkedSubstSymbols;
    std::vector<std::string> linkedReactSymbols;
    std::vector<std::string> linkedSubstClasses;
    std::vector<std::string> linkedSubstIds;
    double time = 0.;
    std::string errorMessage;
};

enum outType
{
    csv = 0,
    transposed,
    propertygrid
};

struct OutputOptions
{
    bool calcSubstFromReact = false;
    bool calcReactFromSubst = false;
    bool output_number_format = false;
    bool outSolventProp = false;
    outType out = outType::csv;
};

/// Widget to work with CorrPT data
class ThermoFunWidgetNew : public jsonui17::BaseWidget
{
    Q_OBJECT

    friend class ThermoFunPrivateNew;

protected slots:

    void typeChanged(const QString& text);

public slots:

    // internal slots
    void openRecordKey(  const std::string& , bool  = false  )  override {}
    void openRecordKey(  const QModelIndex&  ) {}
    void changeKeyList() override {}
    void objectChanged() override {}

    // File
    void CmResetThermoFunData();
    void CmExportCFG();
    void CmImportCFG();

    //Edit
    void CmSelectThermoDataSet();
    void CmSelectSourceTDBs();
    void CmSelectSubstances();
    void CmSelectReactions();
    void CmResetTP();
    void CmReallocTP();
    void CmResetProperty();
    //temporaly
    void CmSetElementsReactions();
    void CmSetElementsReactionSets();

    // Calc
    //void CmCalcMTPARM();
    void CmCalcMTPARM_load();
    void CmCalcMTPARM_calculate();
    void CmCalcMTPARM_finish();

    //Result
    void CmShowResult();

public:

    explicit ThermoFunWidgetNew( QWidget *parent = nullptr);
    ~ThermoFunWidgetNew();

    void setQueryEdited( jsonui17::QueryWidget* ) override {}

private:

    Ui::ThermoFunWidget *ui;
    std::unique_ptr<ThermoFunPrivateNew> pdata;
    // The future watcher that provides monitoring for the currently loading
    QFutureWatcher<ThermoLoadData> loadingWatcher;
    // The future watcher that provides monitoring for the currently calculating
    QFutureWatcher<std::string> calcWatcher;


    // Extern windows
    jsonui17::TableEditWidget* _csvWin = nullptr;
    WaitingSpinnerWidget *waitDialog;

    bool calcSubstFromReact() const;
    bool calcReactFromSubst() const;

    // Internal functions ------------------------

    /// Reset new ThermoFun data and update editors
    void resetThermoFunData( const ThermoFunData& newdata );
    /// Set up menu commands
    void setActions();

    void resetTypeBox( const QString& text );
    /// Reset Solvewnts Checkbox
    void resetSolvents( const jsonio17::values_table_t&  solventValues );

    void closeEvent(QCloseEvent* e) override;

    /// Update menu after change preferences
    virtual void updateViewMenu() override {}
    /// Change model ( show/hide comments)
    virtual void updateModel() override {}
    /// Change table (Show Enum Names Instead of Values)
    virtual void updateTable() override {}
    /// Update internal lists after change DB locations
    virtual void updateDB() override;

    virtual void CmHelpAbout() override;
    virtual void CmHelpAuthors() override;
    virtual void CmHelpLicense() override;

};


#endif // ThermoFunWINDOW_H
