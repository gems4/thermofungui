//  This is ThermoFun library+API (https://bitbucket.org/gems4/ThermoFun)
//
/// \file ThermoFunWidget.cpp
/// ThermoFunWidget - Widget to work with ThermoFun data
//
// JSONUI is a C++ Qt5-based widget library and API aimed at implementing
// the GUI for editing/viewing the structured data kept in a NoSQL database,
// with rendering documents to/from JSON/YAML/XML files, and importing/
// exporting data from/to arbitrary foreign formats (csv etc.). Graphical
// presentation of tabular data (csv format fields) is also possible.
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONUI depends on the following open-source software products:
// jsonio (https://bitbucket.org/gems4/jsonio); Qt5 (https://qt.io);
// Qwtplot (http://qwt.sourceforge.net).

#include <iostream>
#include <QMessageBox>
#include <QKeyEvent>
#include <QInputDialog>
#ifdef _WIN32
#include <chrono>
#else
#include <sys/time.h>
#endif
#include <QtConcurrent>
#include "ThermoFunPrivateNew.h"
#include "ui_ThermoFunWidget.h"
#include "TPSetDialog.h"
#include "SelectThermoDataDialog.h"
#include "jsonui17/TableEditWindow.h"
#include "jsonui17/service/SchemaSelectDialog.h"
#include "jsonui17/service/waitingspinnerwidget.h"
#include "jsonui17/HelpMainWindow.h"
#include "jsonui17/FileDialogs.h"
#include "jsonui17/preferences.h"
#include "jsonio17/dbvertexdoc.h"

// ThermoFun includes
#ifdef FROM_SRC
#include "../ThermoHubClient/ReactionData.h"
#include "../ThermoHubClient/ReactionSetData.h"
#else
#include "ThermoHubClient/ReactionData.h"
#include "ThermoHubClient/ReactionSetData.h"
#endif

/// Default logger for ThermoFunGui library
extern std::shared_ptr<spdlog::logger> fungui_logger;


ThermoFunWidgetNew::ThermoFunWidgetNew( QWidget *parent) :
    jsonui17::BaseWidget( "", parent),
    ui(new Ui::ThermoFunWidget), _csvWin(nullptr)
{
    // set up widget data
    ui->setupUi(this);
    ui->keySplitter->setStretchFactor(0, 1);
    ui->mainSplitter->setCollapsible(0, false);
    ui->mainSplitter->setCollapsible(1, false);

    //define schema checkbox
    ui->typeBox->addItem("Substances");
    ui->typeBox->addItem("Reactions");

    setAttribute(Qt::WA_DeleteOnClose); // automatically delete itself when window is closed
    QString title = qApp->applicationName()+" application for tabulating standard state thermodynamic properties";
    setWindowTitle(title);

    // init internal data
    pdata.reset( new ThermoFunPrivateNew(this) );
    resetThermoFunData( pdata->data() );

    // define menu
    setActions();
    waitDialog = new WaitingSpinnerWidget(this, true/*Qt::ApplicationModal*/, true);

    //show();
//    CmSelectThermoDataSet();
}

ThermoFunWidgetNew::~ThermoFunWidgetNew()
{
    // Cancel and wait
    if (loadingWatcher.isRunning())
    {
        loadingWatcher.cancel();
        loadingWatcher.waitForFinished();
    }

    if (calcWatcher.isRunning())
    {
        calcWatcher.cancel();
        calcWatcher.waitForFinished();
    }

    if( _csvWin )
    {
       _csvWin->close();
       delete _csvWin;
    }
    if( waitDialog )
        delete waitDialog;

    delete ui;
}

void ThermoFunWidgetNew::closeEvent(QCloseEvent* e)
{
    if( _csvWin )
     _csvWin->close();
    BaseWidget::closeEvent(e);
}

void ThermoFunWidgetNew::updateDB()
{
    // clear internal data
    pdata->ThermoHubClient.setDBElements();
    resetThermoFunData( pdata->data() );
}

//  Connect all actions
void ThermoFunWidgetNew::setActions()
{
    connect( ui->typeBox, SIGNAL(currentTextChanged(const QString&)),
             this, SLOT(typeChanged(const QString&)));

    // File
    connect( ui->actionNew_Reset_Task_Definition, SIGNAL( triggered()), this, SLOT(CmResetThermoFunData()));
    connect( ui->actionE_xit, SIGNAL( triggered()), this, SLOT(close()));
    connect( ui->actionExport_CfgFile, SIGNAL( triggered()), this, SLOT(CmExportCFG()));
    connect( ui->actionImport_CfgFile, SIGNAL( triggered()), this, SLOT(CmImportCFG()));

    // Edit
    connect(ui->actionRealloc_TP, SIGNAL(triggered()), this, SLOT(CmReallocTP()));
    connect(ui->actionReset_TP, SIGNAL(triggered()), this, SLOT(CmResetTP()));
    connect(ui->actionChange_Property_list, SIGNAL(triggered()), this, SLOT(CmResetProperty()));
    connect(ui->actionSelect_ThermoDataSet, SIGNAL(triggered()), this, SLOT(CmSelectThermoDataSet()));
    connect(ui->actionSelect_Elements, SIGNAL(triggered()), this, SLOT(CmSelectSourceTDBs()));
    connect(ui->actionSelect_Substances, SIGNAL(triggered()), this, SLOT(CmSelectSubstances()));
    connect(ui->actionSelect_Reactions, SIGNAL(triggered()), this, SLOT(CmSelectReactions()));


    connect(ui->action_Set_Elemets_to_reactions, SIGNAL(triggered()), this, SLOT(CmSetElementsReactions()));
    connect(ui->action_Set_Elemets_to_reactionsets, SIGNAL(triggered()), this, SLOT(CmSetElementsReactionSets()));

    // Help
    connect( ui->action_Help_About, SIGNAL( triggered()), this, SLOT(CmHelpAbout()));
    connect( ui->actionContents, SIGNAL( triggered()), this, SLOT(CmHelpContens()));
    connect( ui->actionAuthors, SIGNAL( triggered()), this, SLOT(CmHelpAuthors()));
    connect( ui->actionLicense, SIGNAL( triggered()), this, SLOT(CmHelpLicense()));

    //TCorpt data
    connect( ui->pName, SIGNAL( textEdited(const QString&)), pdata.get(), SLOT(nameChanged(const QString&)));
    connect( ui->pComment, SIGNAL( textEdited(const QString&)), pdata.get(), SLOT(commentChanged(const QString&)));
    connect( ui->pTunit, SIGNAL( currentTextChanged(const QString&)), pdata.get(), SLOT(TUnitsChanged(const QString&)));
    connect( ui->pPunit, SIGNAL( currentTextChanged(const QString&)), pdata.get(), SLOT(PUnitsChanged(const QString&)));
    connect( ui->pOutput, SIGNAL( currentTextChanged(const QString&)), pdata.get(), SLOT(POutputChanged(const QString&)));
    connect( ui->pPrecision, SIGNAL( valueChanged(int)), pdata.get(), SLOT(pPChanged(int)));
    connect( ui->tPrecision, SIGNAL( valueChanged(int)), pdata.get(), SLOT(tPChanged(int)));

    //Calc
    connect( ui->actionCalculate_Properties, SIGNAL( triggered()), this, SLOT(CmCalcMTPARM_load()));
    connect(&loadingWatcher, &QFutureWatcher<ThermoLoadData>::finished, this, &ThermoFunWidgetNew::CmCalcMTPARM_calculate );
    connect(&calcWatcher, &QFutureWatcher<std::string>::finished, this, &ThermoFunWidgetNew::CmCalcMTPARM_finish );
    connect( ui->actionShow_Results, SIGNAL( triggered()), this, SLOT(CmShowResult()));

    ui->actionCalculate_Reactions_Records_from_Reactants->setChecked(false);
    ui->actionCalculate_Reactions_Records_from_Reactants->setEnabled(false);

    ui->actionReset_TP->setEnabled(false);
    ui->actionRealloc_TP->setEnabled(false);
    ui->actionChange_Property_list->setEnabled(false);
    ui->actionCalculate_Properties->setEnabled(false);
    ui->actionShow_Results->setEnabled(false);
    ui->actionSelect_Substances->setEnabled(false);
    ui->actionSelect_Reactions->setEnabled(false);
    ui->actionSelect_ReactionSets->setEnabled(false);

    ui->nameToolBar->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
}


//-----------------------------------------------------

void ThermoFunWidgetNew::resetTypeBox( const QString& text )
{
    disconnect( ui->typeBox, SIGNAL(currentTextChanged(const QString&)),
             this, SLOT(typeChanged(const QString&)));
    ui->typeBox->setCurrentText(text);
    connect( ui->typeBox, SIGNAL(currentTextChanged(const QString&)),
             this, SLOT(typeChanged(const QString&)));
}

// Reset ThermoFun data
void ThermoFunWidgetNew::resetThermoFunData( const ThermoFunData& newdata )
{
    pdata->newThermoFunData(newdata);

    // update view
    ui->pName->setText(newdata.name.c_str());
    ui->pComment->setText(newdata.comment.c_str());
    ui->pTunit->setCurrentText( newdata.unitsT.c_str());
    ui->pPunit->setCurrentText(newdata.unitsP.c_str());
    ui->pOutput->setCurrentText(newdata.output.c_str());
    ui->pPrecision->setValue(newdata.pPrecision);
    ui->tPrecision->setValue(newdata.tPrecision);
    ui->calcStatus->setText(newdata.calcStatus.c_str());
    ui->actionShow_Results->setEnabled(false);
    ui->actionCalculate_Properties->setEnabled(false);
    ui->actionChange_Property_list->setEnabled(false);
    ui->typeBox->setEnabled(false);
    ui->actionReset_TP->setEnabled(false);
    ui->actionSelect_Substances->setEnabled(false);
    ui->actionSelect_Reactions->setEnabled(false);
    ui->pSolventSymbol->clear();
    resetTypeBox( newdata.schemaName.c_str() );
}

void ThermoFunWidgetNew::resetSolvents( const jsonio17::values_table_t&  solventValues )
{
    std::string sSymbol, sId;
    auto substDataNdx = pdata->getSubstDataIndex();
    ui->pSolventSymbol->clear();

    if (solventValues.size() == 0)
      ui->pSolventSymbol->addItem("No solvent!", 0);

    for( auto solvent: solventValues )
     {
       sSymbol = solvent[substDataNdx["symbol"]];
       sId = solvent[substDataNdx["_id"]];
       ui->pSolventSymbol->addItem(sSymbol.c_str(), sId.c_str());
    }
}

bool ThermoFunWidgetNew::calcSubstFromReact() const
{
  return  ui->actionCalculate_Substances_Records_from_Dependent_Reactions->isChecked();
}

bool ThermoFunWidgetNew::calcReactFromSubst() const
{
  return  ui->actionCalculate_Reactions_Records_from_Reactants->isChecked();
}

// Menu commands -----------------------------------------------------------

void ThermoFunWidgetNew::CmResetThermoFunData()
{
    ThermoFunData dt;
    dt.resetSchemaName( pdata->data().schemaName );
    resetThermoFunData(dt);
}

// Change current Vertex
void ThermoFunWidgetNew::typeChanged(const QString& text)
{
  try {
        std::string newname = text.toStdString();
        if (newname == "Substances")
        {
            pdata->typeChanged( "VertexSubstance" );
            ui->actionCalculate_Substances_Records_from_Dependent_Reactions->setEnabled(true);
//            ui->actionCalculate_Reactions_Records_from_Reactants->setChecked(false);
            ui->actionCalculate_Reactions_Records_from_Reactants->setEnabled(false);

            ui->actionSelect_Substances->setEnabled(true);
            ui->actionSelect_Reactions->setEnabled(false);
        }
        if (newname == "Reactions")
        {
            pdata->typeChanged( "VertexReaction" );
//            ui->actionCalculate_Substances_Records_from_Dependent_Reactions->setChecked(false);
//            ui->actionCalculate_Substances_Records_from_Dependent_Reactions->setEnabled(false);
            ui->actionCalculate_Reactions_Records_from_Reactants->setEnabled(true);
            ui->actionSelect_Substances->setEnabled(false);
            ui->actionSelect_Reactions->setEnabled(true);
        }
    }
   catch(jsonio17::jsonio_exception& e)
   {
       QMessageBox::critical( this, "jsonio_exception", e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void ThermoFunWidgetNew::CmSelectThermoDataSet()
{
  try {
       SelectThermoDataDialog dlg( pdata->data().idThermoDataSet, pdata->data().elements, pdata->ThermoHubClient, this);

      if( dlg.exec() )
      {
        std::vector<ChemicalFun::ElementKey> elKeys;
        dlg.allSelected( elKeys );
        // Reset internal data
        pdata->updateData( dlg.idThermoDataSet(), dlg.sourceTDBs(), elKeys,
                           dlg.getSubstanceValues(), dlg.getReactionValues() );
        resetSolvents( dlg.getSolventValues() );

//        ui->actionReset_TP->setEnabled(true);
//        ui->actionRealloc_TP->setEnabled(true);
//        ui->actionChange_Property_list->setEnabled(true);
//        ui->actionCalculate_Properties->setEnabled(true);
        ui->typeBox->setEnabled(true);
//        ui->actionSelect_Substances->setEnabled(true);
//        ui->actionSelect_ThermoDataSet->setEnabled(true);
        ui->calcStatus->setText("Select substances or reactions, set temperature and pressure points, properties to calculate, and click calculate."); // status
        typeChanged(ui->typeBox->currentText());
      }
//      else {
//          ui->actionRealloc_TP->setEnabled(false);
//          ui->actionChange_Property_list->setEnabled(false);
//          ui->actionCalculate_Properties->setEnabled(false);
//          ui->typeBox->setEnabled(false);
//          pdata->
//          ui->actionSelect_Substances->setEnabled(false);
//          ui->actionSelect_Reactions->setEnabled(false);
//          ui->actionSelect_ThermoDataSet->setEnabled(true);
//      }
    }
   catch(jsonio17::jsonio_exception& e)
   {
       QMessageBox::critical( this, "jsonio_exception", e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void ThermoFunWidgetNew::CmSelectSourceTDBs()
{
  try {
       SelectThermoDataDialog dlg( pdata->data().sourceTDBs, pdata->data().elements, pdata->ThermoHubClient, this);

      if( dlg.exec() )
      {
        std::vector<ChemicalFun::ElementKey> elKeys;
        dlg.allSelected( elKeys );
        // Reset internal data
        pdata->updateData( "", dlg.sourceTDBs() ,elKeys,
                           dlg.getSubstanceValues(), dlg.getReactionValues() );
        resetSolvents( dlg.getSolventValues() );

        ui->actionReset_TP->setEnabled(true);
        ui->actionRealloc_TP->setEnabled(true);
        ui->actionChange_Property_list->setEnabled(true);
        ui->actionCalculate_Properties->setEnabled(true);
        ui->typeBox->setEnabled(true);
        ui->calcStatus->setText("Set temperature and pressure points, set properties to calculate, and click calculate."); // status
      }
    }
   catch(jsonio17::jsonio_exception& e)
   {
       QMessageBox::critical( this, "jsonio_exception", e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void ThermoFunWidgetNew::CmReallocTP()
{
  try{
        bool ok = 0;
        auto size = QInputDialog::getInt( this, "Please, select new TP pairs array size",
                 "Array size ", pdata->data().tppairs.size(), 0, 999, 1, &ok );
        if(ok)
          pdata->reallocTP( size );
    }
   catch(jsonio17::jsonio_exception& e)
   {
       QMessageBox::critical( this, "jsonio_exception", e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void ThermoFunWidgetNew::CmResetTP()
{
  try{
        TPSetDialog dlg( this);
        if( dlg.exec() )
           pdata->updateTP( dlg.getTUnits().toStdString(),
                  dlg.getPUnits().toStdString(), dlg.getTPpairs() );
    }
   catch(jsonio17::jsonio_exception& e)
   {
       QMessageBox::critical( this, "jsonio_exception", e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void ThermoFunWidgetNew::CmResetProperty()
{
 try
  {
    std::string schemaName = pdata->getThermoPropertiesName();
    jsonui17::SchemaSelectDialog dlg(this, "Please, mark property fields",
         schemaName, pdata->data().properties);
    if( dlg.exec() )
      pdata->updateProperty( dlg.allSelected() );
  }
   catch(jsonio17::jsonio_exception& e)
   {
       QMessageBox::critical( this, "jsonio_exception", e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }

}

/// Read bson record from json file fileName
void ThermoFunWidgetNew::CmImportCFG()
{
    try{
        std::string fileName;

        if(  jsonui17::ChooseFileOpen( this, fileName,
                                       "Please, select Structured Data file", jsonui17::jsonFilters  ))
        {
            // read data to dom

            jsonio17::JsonFile file( fileName );
            auto domdata = jsonio17::JsonFree::object();
            file.load(domdata);

            ThermoFunData thdata;
            thdata.fromJsonNode(domdata);
            resetThermoFunData(thdata);
        }
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write current task to configuration file file
void ThermoFunWidgetNew::CmExportCFG()
{
    try {
        std::string fileName = pdata->data().name+".json";
        if(  jsonui17::ChooseFileSave( this, fileName,
                                       "Please, select file to write the data", jsonui17::jsonFilters  ))
        {
            jsonio17::JsonFile file( fileName );
            auto domdata = jsonio17::JsonFree::object();
            pdata->data().toJsonNode( domdata );
            file.save( domdata );
        }
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


void ThermoFunWidgetNew::CmCalcMTPARM_load()
{
    try {
        //          MapSymbolMapLevelReaction   levelDefinesReaction  = pdata->recordsMapLevelDefinesReaction(/*3, 0*/);
        // Now we use all into window
        // select components
        const jsonio17::values_table_t& values= pdata->getValues( pdata->isSubstances() );
        std::vector<size_t> selNdx;
        for(size_t ii=0; ii< values.size(); ii++)
            selNdx.push_back(ii);
        // Run into other thread
#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
        loadingWatcher.setFuture(QtConcurrent::run( pdata.get(), &ThermoFunPrivateNew::loadData, selNdx));
#else
        loadingWatcher.setFuture(QtConcurrent::run( &ThermoFunPrivateNew::loadData, pdata.get(), selNdx));
#endif
        waitDialog->start();
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


void ThermoFunWidgetNew::CmCalcMTPARM_calculate()
{
    try {
        waitDialog->stop();
        fungui_logger->debug(" Finished : {}", loadingWatcher.result().errorMessage);

        auto loadData = loadingWatcher.result();

        // test error when loading
        if( !loadData.errorMessage.empty() )
        {
            ui->calcStatus->setText("Data loading error.");
            QMessageBox::critical( this, "Data loading error", loadData.errorMessage.c_str() );
            return;
        }

        if (calcReactFromSubst() && calcSubstFromReact()) // check - reaction with substance dependent on another reaction
        {
            MapSymbolMapLevelReaction   levelDefinesReaction  =
                    pdata->recordsMapLevelDefinesReaction(loadData.linkedSubstIds, loadData.linkedSubstSymbols);
            for (const auto& substSymbol : loadData.linkedSubstSymbols)
            {
                MapLevelReaction    levelReaction   = levelDefinesReaction[substSymbol];
                if (levelReaction.size() == 0)
                    continue;
                if (levelReaction.size() == 1)
                {
                    pdata->setSubstanceLevel(substSymbol, levelReaction.begin()->first);
                }
                if (levelReaction.size() > 1)
                {
                    jsonio17::values_table_t values_;
                    std::vector<std::string> levels;
                    for (const auto& react : levelReaction)
                    {
                        std::vector<std::string> symbolEq;
                        symbolEq.push_back(react.second.symbol());
                        symbolEq.push_back(react.second.equation());
                        levels.push_back(react.first);
                        values_.push_back(symbolEq);
                    }

                    jsonui17::SelectDialog selDlg_lvl( false, this, ("Please, select the reaction which defines substance: "+substSymbol).c_str(), values_ );
                    if( !selDlg_lvl.exec() )
                        return;
                    auto solvNdx =  selDlg_lvl.getSelectedIndex();

                    pdata->setSubstanceLevel(substSymbol, levels[solvNdx]);
                }
            }
        }

        // check for solvent
        std::string solventSymbol = ui->pSolventSymbol->currentText().toStdString();
        if (solventSymbol != "No solvent!")
        {
            auto sId = ui->pSolventSymbol->currentData().toString().toStdString();
            if (std::find(loadData.substKeys.begin(), loadData.substKeys.end(), sId) == loadData.substKeys.end())
                loadData.substKeys.push_back(sId);
        }



        // run in other thread
        OutputOptions options;
        options.out = static_cast<outType>(ui->pOutput->currentIndex());
        options.calcSubstFromReact = calcSubstFromReact();
        options.calcReactFromSubst = calcReactFromSubst();
        options.output_number_format = ui->actionFixed_Outputnumber_format->isChecked();

#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
        calcWatcher.setFuture(QtConcurrent::run( pdata.get(), &ThermoFunPrivateNew::calcData,
                                                 std::move(loadData), std::move(solventSymbol),
                                                 options ));
#else
        calcWatcher.setFuture(QtConcurrent::run( &ThermoFunPrivateNew::calcData, pdata.get(),
                                                 std::move(loadData), std::move(solventSymbol),
                                                 options ));
#endif

        waitDialog->start();
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

void ThermoFunWidgetNew::CmCalcMTPARM_finish()
{
    try {
        waitDialog->stop();

        auto retMessage = calcWatcher.result();
        if( retMessage.find("Click view results") == std::string::npos )
        {
            ui->calcStatus->setText("Calculation finished with error.");
            jsonio17::JSONIO_THROW( "ThermoiFunGUI", 204, "Calculation finished with error: " + retMessage );
        }
        ui->calcStatus->setText(retMessage.c_str());
        ui->actionShow_Results->setEnabled(true);
        CmShowResult();
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

//Result
void ThermoFunWidgetNew::CmShowResult()
{
    try {
        // define new dialog
        std::string fileName = pdata->data().resultsFile;
        if(!_csvWin)
        {
            _csvWin = new jsonui17::TableEditWidget("Tabulated results ", fileName,
                                                    jsonui17::MatrixTable::tbEdit|jsonui17::MatrixTable::tbGraph/*|jsonui17::MatrixTable::tbSort*/ );
        }
        else
        {
            _csvWin->openNewCSV(fileName);
            _csvWin->raise();
        }
        _csvWin->show();
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// temporary function to reset old reactions
void ThermoFunWidgetNew::CmSetElementsReactions()
{
    try {

        //auto graphdb = pdata->ThermoHubClient.reactData().getDB();
        std::shared_ptr<jsonio17::DBVertexDocument> graphdb( jsonio17::DBVertexDocument::newVertexDocumentQuery(
                              jsonui17::uiSettings().database(), "VertexReaction" ));

        std::vector<std::string> sel_keys = jsonui17::SelectKeysFrom( graphdb.get(), this, "Please, select a record to update" );
        for( size_t ii=0; ii<sel_keys.size(); ii++ )
            pdata->resetElementsintoRecord( true, sel_keys[ii]);

    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// temporary function to reset old reactionSets
void ThermoFunWidgetNew::CmSetElementsReactionSets()
{
    try {
        //auto graphdb = pdata->ThermoHubClient.reactSetData().getDB();
        std::unique_ptr<jsonio17::DBVertexDocument> graphdb( jsonio17::DBVertexDocument::newVertexDocumentQuery(
                                                                 jsonui17::uiSettings().database(), "VertexReactionSet" ));

        std::vector<std::string> sel_keys = jsonui17::SelectKeysFrom( graphdb.get(), this, "Please, select a record to update" );
        for( size_t ii=0; ii<sel_keys.size(); ii++ )
            pdata->resetElementsintoRecord(false, sel_keys[ii]);

    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
    }
}


void ThermoFunWidgetNew::CmSelectSubstances()
{
    try{
        std::size_t colId = pdata->ThermoHubClient.substData().getDataName_DataIndex().at("_id");
        std::vector<std::string> oldids = pdata->substModel->getColumn( colId );
        // read full list
        pdata->substModel->loadModeRecords( pdata->substValues );

        // select components
        const jsonio17::values_table_t& values= pdata->getValues( pdata->isSubstances() );
        std::vector<size_t> selNdx = pdata->substModel->recordToValues( colId, oldids );
        jsonui17::SelectDialog selDlg( true, this, "Please, select records for calculating their properties",
                                       values, pdata->ThermoHubClient.substData().getDataHeaders(),
                                       jsonui17::MatrixTable::tbNoMenu|jsonui17::MatrixTable::tbSort );
        selDlg.setSelection(selNdx);
        std::set<size_t> selNdx2;
        if( selDlg.exec() )
        {
          selNdx2 = selDlg.allSelected();
        }
        else
        {
           selNdx2.insert(selNdx.begin(), selNdx.end());
        }
        pdata->substModel->leftOnlySelected( selNdx2 );

        ui->actionReset_TP->setEnabled(true);
        ui->actionRealloc_TP->setEnabled(true);
        ui->actionChange_Property_list->setEnabled(true);
        ui->actionCalculate_Properties->setEnabled(true);

        ui->calcStatus->setText("Set temperature and pressure points, set properties to calculate, and click calculate. (see preferences)"); // status
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
        ui->calcStatus->setText(e.what());
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
        ui->calcStatus->setText(e.what());
    }
}


void ThermoFunWidgetNew::CmSelectReactions()
{
    try{
        std::size_t colId = pdata->ThermoHubClient.reactData().getDataName_DataIndex().at("_id");
        std::vector<std::string> oldids = pdata->reactModel->getColumn( colId );
        // read full list
        pdata->reactModel->loadModeRecords( pdata->reactValues );

        // select components
        jsonio17::values_table_t values= pdata->reactModel->getValues();
        std::vector<size_t> selNdx = pdata->reactModel->recordToValues( colId, oldids );

        jsonui17::SelectDialog selDlg( true, this, "Please, select records for calculating their properties",
                                       values, pdata->ThermoHubClient.reactData().getDataHeaders(),
                                       jsonui17::MatrixTable::tbNoMenu|jsonui17::MatrixTable::tbSort );
        selDlg.setSelection(selNdx);
        std::set<size_t> selNdx2;
        if( selDlg.exec() )
        {
          selNdx2 = selDlg.allSelected();
        }
        else
        {
           selNdx2.insert(selNdx.begin(), selNdx.end());
        }
        pdata->reactModel->leftOnlySelected( selNdx2 );

        ui->actionReset_TP->setEnabled(true);
        ui->actionRealloc_TP->setEnabled(true);
        ui->actionChange_Property_list->setEnabled(true);
        ui->actionCalculate_Properties->setEnabled(true);

        ui->calcStatus->setText("Set temperature and pressure points, set properties to calculate, and click calculate. (see preferences)"); // status
    }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
        ui->calcStatus->setText(e.what());
    }
    catch(std::exception& e)
    {
        QMessageBox::critical( this, "std::exception", e.what() );
        ui->calcStatus->setText(e.what());
    }
}

void ThermoFunWidgetNew::CmHelpAbout()
{
    jsonui17::helpWin("AboutThermoFunGUI","");
}

void ThermoFunWidgetNew::CmHelpAuthors()
{
    jsonui17::helpWin("AuthorsThermoFunGUI","");
}

void ThermoFunWidgetNew::CmHelpLicense()
{
    jsonui17::helpWin("LicenseThermoFunGUI","");
}

// end of ThermoFunMenu.cpp


