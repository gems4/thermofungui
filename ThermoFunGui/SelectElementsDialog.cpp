#include <iostream>
#include <QMessageBox>
#include <QHeaderView>
#include "SelectElementsDialog.h"
#include "ElementsWidget.h"
#include "ui_SelectElementsDialog.h"
#include "jsonui17/preferences.h"
#include "thermomodel.h"
// ThermoFun includes
#ifdef FROM_SRC
#include "../ThermoHubClient/DatabaseClient.h"
#include "../ThermoHubClient/ReactionSetData.h"
#include "../ThermoHubClient/SubstanceData.h"
#endif
#ifndef FROM_SRC
#include "ThermoHubClient/DatabaseClient.h"
#include "ThermoHubClient/ReactionSetData.h"
#include "ThermoHubClient/SubstanceData.h"
#endif
using namespace std;


struct SelectElementsDialogPrivate
{
   SelectElementsDialog* window;

   /// Selected SourceTDB
   int sourceTDB = -1;
   /// Define ELEMENTS table data
   vector<ChemicalFun::ElementKey> elementsRow;
   /// Connect to ReactionSet record
   ThermoFun::DatabaseClient ThermoHubClient;
//   ReactionSetData rcSetData;
   std::shared_ptr<ThermoColorStringContainer> rcsetData;
   std::shared_ptr<jsonui17::MatrixModel>  rcsetModel;

   /// Current id of ReactionSet
   string idReactopnSet = "";

// ---------------------------------------------

   SelectElementsDialogPrivate(SelectElementsDialog* awindow, const ThermoFun::DatabaseClient& hubClient):
    window(awindow), ThermoHubClient(hubClient)
   {
       rcsetData.reset( new ThermoColorStringContainer(&ThermoHubClient.reactSetData()));
       rcsetModel.reset( new jsonui17::MatrixModel( rcsetData ) );
   }

   virtual ~SelectElementsDialogPrivate()
   { }

   void clearAll()
   {
     elementsRow.clear();
   }

   vector<string> getSourcetdbList()
   {
       return ThermoHubClient.sourcetdbListAll();
   }

   bool makeAvailableElementsList( int sourcendx )
   {
      if( sourceTDB == sourcendx ) // Source did not chaged
          return false;
#ifndef _MSC_VER
      elementsRow = ThermoHubClient.availableElementsKey(sourcendx);
#endif
      sourceTDB = sourcendx;
      return true;
   }

   const vector<ChemicalFun::ElementKey>& allAvailableElementsList() const
   {
     return elementsRow;
   }

   int loadReacSetRecords( const vector<ChemicalFun::ElementKey>& elements )
   {
     auto matr = ThermoHubClient.reactSetData().loadRecordsValues(jsonio17::DBQueryBase::emptyQuery(), sourceTDB, elements);

// here
  ///  auto jsonT = ThermoHubClient.substData().getJsonBsonRecordVertex("59a7dcedf3830548000000e7:").first;
// here
     matr.insert( matr.begin(), {"All","", "", "",""});
     rcsetData->updateValues(matr);
     rcsetModel->resetMatrixData();
     if( !idReactopnSet.empty() )
     {
       for( std::size_t ii=1; ii<matr.size(); ii++)
        if( idReactopnSet == matr[ii][4] )
         return ii;
     }
     return 0;
   }

   string getReactionSetId(size_t ndx) const
   {
     return  rcsetData->get_data(ndx, 4);
   }
};


//===========================================================================


SelectElementsDialog::SelectElementsDialog(const ThermoFun::DatabaseClient& hubClient, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SelectElements), cont_data(nullptr), pTable(0),
    pdata(new SelectElementsDialogPrivate(this, hubClient))
{
    ui->setupUi(this);

    ui->stackedWidget->setCurrentIndex(0);
    resetNextButton();
    resetBackButton();

    // init elems
    elmsWidget = new ElementsWidget( ui->page_2 );
    ui->gridLayout_2->addWidget(elmsWidget);

    defineSource();
    defineReactionSets();

    // signals and slots connections
    connect( ui->bBack, SIGNAL(clicked()), this, SLOT(CmBack()));
    connect( ui->bHelp, SIGNAL( clicked() ), this, SLOT( CmHelp() ) );
    connect( ui->bReset, SIGNAL( clicked() ), this, SLOT( CmReset() ) );
    connect( ui->bSelectAll, SIGNAL( clicked() ), this, SLOT( CmSelectAll() ) );
    connect( ui->pCancelButton, SIGNAL( clicked() ),this, SLOT( reject()  ) );
}

SelectElementsDialog::~SelectElementsDialog()
{
    delete pTable;
    delete elmsWidget;
    delete ui;
}


//------------------- Actions

void SelectElementsDialog::CmBack()
{
    ui->stackedWidget->setCurrentIndex( ui->stackedWidget->currentIndex()-1 );
    resetNextButton();
    resetBackButton();
}

void SelectElementsDialog::CmNext()
{
  try
  {
    if(ui->stackedWidget->currentIndex()==0)
    {
       int ndx = pTable->currentIndex().sibling(pTable->currentIndex().row(),0).data().toInt();
       defineElements( ndx );
    }
    if(ui->stackedWidget->currentIndex()==1)
    {
      // select
       updateReactionSets();
    }
    ui->stackedWidget->setCurrentIndex ( ui->stackedWidget->currentIndex()+1 );
    resetNextButton();
    resetBackButton();
  }
    catch(jsonio17::jsonio_exception& e)
    {
        QMessageBox::critical( this, "jsonio_exception", e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( this, "std::exception", e.what() );
     }
}

void SelectElementsDialog::CmOk()
{
   accept();
}


void SelectElementsDialog::CmSelectAll()
{
  switch(  ui->stackedWidget->currentIndex() )
  {
    case 1:  elmsWidget->CmSelectAll();
             break;
    default:
             break;
  }
}


void SelectElementsDialog::CmReset()
{
  switch(  ui->stackedWidget->currentIndex() )
  {
    case 1:  elmsWidget->CmReset();
             break;
    default:
             break;
  }
}

//--------------------------------------------------------------------

void SelectElementsDialog::defineSource()
{
  vector<string> _list = pdata->getSourcetdbList();

  cont_data = std::make_shared<jsonui17::SelectTableContainer>("select", _list, '-');
  jsonui17::MatrixModel* model = new jsonui17::MatrixModel( cont_data, this );
  pTable = new jsonui17::MatrixTable(this, jsonui17::MatrixTable::tbNoMenu );
  pTable->setModel(model);
  ui->gridLayout->addWidget(pTable, 2, 0, 1, 1);
  pTable->horizontalHeader()->hide();
  pTable->setSelectionBehavior(QAbstractItemView::SelectRows);
  pTable->setSelectionMode(QAbstractItemView::SingleSelection);
  pTable->setCurrentIndex( pTable->model()->index(0,0) );
  pTable->selRow();
}

void  SelectElementsDialog::defineElements( int sourcetdb )
{
   if( pdata->makeAvailableElementsList(sourcetdb))
   {
       const vector<ChemicalFun::ElementKey>& elements = pdata->allAvailableElementsList();
       fungui_logger->debug(" elements {}", ChemicalFun::to_string(elements));
       elmsWidget->setElementList(elements, pdata->ThermoHubClient.availableElements());
   }
}

void  SelectElementsDialog::defineReactionSets()
{
    rcsetTable = new jsonui17::MatrixTable( this );
    rcsetTable->setModel(pdata->rcsetModel.get());
    rcsetTable->horizontalHeader()->setSectionResizeMode( QHeaderView::Interactive );
    rcsetTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    rcsetTable->setSelectionMode(QAbstractItemView::SingleSelection);
    QObject::disconnect( rcsetTable, SIGNAL(customContextMenuRequested(QPoint)),
           rcsetTable, SLOT(slotPopupContextMenu(QPoint)));
    ui->verticalLayout->addWidget(rcsetTable);
}

void   SelectElementsDialog::updateReactionSets()
{
    vector<ChemicalFun::ElementKey> elementKeys;
    allSelected( elementKeys );
    int curndx = pdata->loadReacSetRecords( elementKeys );
    rcsetTable->setCurrentIndex(rcsetTable->model()->index( curndx,0 ));
    rcsetTable->selRow();
}

// -- Returns selection array
void SelectElementsDialog::allSelected( vector<ChemicalFun::ElementKey>& elementKeys ) const
{
    elementKeys.clear();
    string idrc = idReactionSet();
    if( !idrc.empty() )
    {
       auto ellst = pdata->ThermoHubClient.reactSetData().getElementsList(idrc);
       elementKeys.insert( elementKeys.end(), ellst.begin(), ellst.end() );
       return;
    }
    elmsWidget->allSelected( elementKeys );
}

int SelectElementsDialog::sourceTDB() const
{
    return pdata->sourceTDB;
}

string SelectElementsDialog::idReactionSet() const
{
  int ndx = rcsetTable->currentIndex().row();
  return pdata->getReactionSetId( ndx );
}


void SelectElementsDialog::setData( int sourcetdb,
         const vector<ChemicalFun::ElementKey>& elementKeys, const string& idRcSet )
{
   if( elementKeys.empty() )
     return;

   pdata->idReactopnSet = idRcSet;
   auto index = pTable->currentIndex();
   for( int ii =0;  ii< pTable->model()->rowCount(); ii++ )
   {
     index = index.sibling( ii,0 );
     if( index.data().toInt() == sourcetdb )
     {
       pTable->setCurrentIndex(index);
       CmNext();
       elmsWidget->selectElementList(elementKeys, pdata->ThermoHubClient.availableElements());
     }
   }
}


// ------------------------ OK old

void 	SelectElementsDialog::resetNextButton()
{

    ui->bReset->setVisible(ui->stackedWidget->currentIndex() == 1 );
    ui->bSelectAll->setVisible(ui->stackedWidget->currentIndex() == 1 );

    if( ui->stackedWidget->currentIndex() == ui->stackedWidget->count() - 1 )
    {
        ui->bNext->disconnect();
        connect( ui->bNext, SIGNAL(clicked()), this, SLOT(CmOk()) );
        ui->bNext->setText("&Finish");
    }
    else
    {
        ui->bNext->disconnect();
        connect( ui->bNext, SIGNAL(clicked()), this, SLOT(CmNext()) );
        ui->bNext->setText("&Next>");
    }
}

void 	SelectElementsDialog::resetBackButton()
{
    ui->bBack->setEnabled( ui->stackedWidget->currentIndex() > 0 );
}

void SelectElementsDialog::CmHelp()
{
  jsonui17::helpWin("HelpSelectSourcetdb", "" );
}


