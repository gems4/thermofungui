#ifndef THERMOMODEL_H
#define THERMOMODEL_H

#include <QObject>
#include "jsonui17/models/table_view.h"
// ThermoFun includes
#ifdef FROM_SRC
#include "../ThermoHubClient/DataContainer.h"
#else
#include "ThermoHubClient/DataContainer.h"
#endif

class TableColorStringContainer: public DataContainer, public jsonui17::ColorAbstractContainer
{

public:

    TableColorStringContainer(const jsonio17::values_t& heads):
        DataContainer(heads), jsonui17::ColorAbstractContainer("select")
    {

    }
    virtual ~TableColorStringContainer()
    { }

    int rowCount() const override
    {
        return row_count();
    }
    int columnCount() const override
    {
        return column_count();
    }
    QVariant data(int line, int column) const override
    {
        return get_data(line, column).c_str();
    }
    bool setData(int line, int column, const QVariant& value) override
    {
        return set_data(line, column, value.toString().toStdString());
    }
    virtual QString headerData(int section) const override
    {
        return header_data(section).c_str();
    }

    Data_Types getType( int line, int column ) const override
    {
        Q_UNUSED( line );
        Q_UNUSED( column );
        return val_type;
    }

    virtual bool isEditable( int line, int column ) const override
    {
        Q_UNUSED( line );
        Q_UNUSED( column );
        return is_editable;
    }

    void resetData() override {}

    void setType( Data_Types new_type )
    {
        val_type = new_type;
    }

    void setEditable( bool editable )
    {
        is_editable = editable;
    }

protected:

    AbstractContainer::Data_Types val_type = AbstractContainer::ftString;
    bool is_editable = false;
};


class ThermoColorStringContainer: public ThermoContainer, public jsonui17::ColorAbstractContainer
{

public:

    ThermoColorStringContainer(ThermoFun::AbstractData *data):
        ThermoContainer(data), jsonui17::ColorAbstractContainer("records")
    {

    }
    virtual ~ThermoColorStringContainer()
    { }

    int rowCount() const override
    {
        return row_count();
    }
    int columnCount() const override
    {
        return column_count();
    }
    QVariant data(int line, int column) const override
    {
        return get_data(line, column).c_str();
    }
    bool setData(int line, int column, const QVariant& value) override
    {
        return set_data(line, column, value.toString().toStdString());
    }
    virtual QString headerData(int section) const override
    {
        return header_data(section).c_str();
    }

    Data_Types getType( int line, int column ) const override
    {
        Q_UNUSED( line );
        Q_UNUSED( column );
        return val_type;
    }

    virtual bool isEditable( int line, int column ) const override
    {
        Q_UNUSED( line );
        Q_UNUSED( column );
        return is_editable;
    }

    void resetData() override {}

    void setType( Data_Types new_type )
    {
        val_type = new_type;
    }

    void setEditable( bool editable )
    {
        is_editable = editable;
    }

protected:

    AbstractContainer::Data_Types val_type = AbstractContainer::ftString;
    bool is_editable = false;
};



class ThermoViewModel: public QObject
{
    Q_OBJECT

    ThermoFun::AbstractData* data_th;
    /// loaded data
    std::shared_ptr<ThermoColorStringContainer> thermoData;
    /// data model
    std::shared_ptr<jsonui17::MatrixModel>  thermoModel;

public:

    ThermoViewModel(ThermoFun::AbstractData *data, QObject* parent = nullptr):
        QObject(parent), data_th(data)
    {
        defineModels();
    }
    virtual ~ThermoViewModel()
    { }

    void linkData(ThermoFun::AbstractData *data)
    {
        data_th = data;
        thermoData->linkData(data_th);
        thermoModel->resetMatrixData();
    }

    void loadModeRecords(const std::string& idReactionSet)
    {
        thermoData->loadModeRecords(idReactionSet);
        thermoModel->resetMatrixData();
    }

    void loadModeRecords(const jsonio17::DBQueryBase& query, int sourcetdb,
                         const std::vector<ChemicalFun::ElementKey>& elements)
    {
        thermoData->loadModeRecords(query, sourcetdb, elements);
        thermoModel->resetMatrixData();
    }

    void loadModeRecords(const std::vector<std::string>& ids)
    {
        thermoData->loadModeRecords(ids);
        thermoModel->resetMatrixData();
    }

    void loadModeRecords(const jsonio17::values_table_t& matr)
    {
        thermoData->loadModeRecords(matr);
        thermoModel->resetMatrixData();
    }

    /// Resize model to selected
    void leftOnlySelected(const std::set<size_t>& selrows)
    {
        thermoData->leftOnlySelected(selrows);
        thermoModel->resetMatrixData();
    }

    /// Move record with values up
    void moveUpByOrder(size_t column, const std::set<std::string>& values)
    {
        thermoData->moveUpByOrder(column, values);
        thermoModel->resetMatrixData();
    }

    jsonio17::values_t getColumn(size_t column, const std::set<size_t>& selrows) const
    {
        return thermoData->getColumn(column, selrows);
    }

    jsonio17::values_t getColumn( size_t column ) const
    {
        return thermoData->getColumn(column);
    }

    template< class T >
    void fillColumn(size_t column, const T& ndxs, const std::string& value)
    {
        thermoData->fillColumn(column, ndxs, value);
        emit thermoModel->dataChanged(thermoModel->index(0, column), thermoModel->index(thermoData->rowCount(), column));
    }

    void fillColumn(size_t column, const std::string& value)
    {
        thermoData->fillColumn(column, value);
        emit thermoModel->dataChanged(thermoModel->index(0, column), thermoModel->index(thermoData->rowCount(), column));
    }

    void fillRow(int row, int start_column, const jsonio17::values_t& values)
    {
        thermoData->fillRow(row, start_column, values);
        emit thermoModel->dataChanged(thermoModel->index(row, 0), thermoModel->index(row, thermoData->columnCount()));
    }

    void fillValue(int row, int column, const std::string& value)
    {
        thermoModel->setData( thermoModel->index(row, column), value.c_str(), Qt::EditRole );
    }


    size_t findRow( size_t column, const std::string& value, bool first = false ) const
    {
        return thermoData->findRow(column, value, first);
    }

    /// Gen indexes of record with value into column
    std::vector<size_t> recordToValue(size_t column, const std::string& value) const
    {
        return thermoData->recordToValue(column, value);
    }


    /// Gen indexes of record with value into column
    std::vector<size_t> recordToValues( size_t column, const std::vector<std::string>& values ) const
    {
        return thermoData->recordToValues(column, values);
    }

    const jsonio17::values_table_t&  getValues() const
    {
        return thermoData->values();
    }

    jsonui17::MatrixModel* getModel() const
    {
        return  thermoModel.get();
    }

    void setColorFunction(jsonui17::GetColor_f func)
    {
        thermoData->setColorFunction(func);
    }

protected:

    void defineModels()
    {
        thermoData.reset( new ThermoColorStringContainer(data_th) );
        thermoModel.reset( new jsonui17::MatrixModel(thermoData) );
    }

};


#endif // THERMOMODEL_H
